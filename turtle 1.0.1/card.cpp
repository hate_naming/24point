#include "card.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QPainter>
#include <QMouseEvent>

Card::Card() {}

Card::Card(CardPoint point, CardColor color) {
    setPoint(point);
    setColor(color);
}

void Card::setPoint(CardPoint point) { mypoint = point; }

void Card::setColor(CardColor color) { mycolor = color; }

Card::CardPoint Card::point() const { return mypoint; }

Card::CardColor Card::color() const { return mycolor; }

bool smallPoint(const Card &c1, const Card &c2)  //由大到小排序
{
    if (c1.point() == c2.point())  //点数相同，判断花色
    {
        return c1.color() < c2.color();
    } else {
        return c1.point() < c2.point();
    }
}

bool bigPoint(const Card &c1, const Card &c2) {
    if (c1.point() == c2.point()) {
        return c1.color() > c2.color();
    } else {
        return c1.point() > c2.point();
    }
}

bool operator==(const Card &left, const Card &right) {
    return (left.point() == right.point() && left.color() == right.color());
}

uint qHash(const Card &card) { return card.point() * 100 + card.color(); }

bool operator<(const Card &c1, const Card &c2) { return smallPoint(c1, c2); }

bool operator>(const Card &c1, const Card &c2) { return bigPoint(c1, c2); }
