#include "player.h"

#include <QDebug>

Player::Player(QObject *parent) : QObject(parent){ mytype = Unknown; }

Player::Player(QString name, QObject *parent) : QObject(parent){
    myname = name;
    mytype = Unknown;
}

Player::Player(QString name, Player::Type playtype, QObject *parent) : QObject(parent){
    myname = name;
    mytype = playtype;
}

void Player::setName(QString name)
{
    myname = name;
}

QString Player::getName()
{
    return myname;
}

void Player::setType(Type type)
{
    mytype = type;
}

Player::Type Player::getType()
{
    return mytype;
}

void Player::setLose(bool flag)
{
    loseFlag = flag;
}

bool Player::isLose()
{
    return loseFlag;
}

void Player::setForePlayer(Player *player)
{
    myfore = player;
}

void Player::setNextPlayer(Player *player)
{
    mynext = player;
}

Player *Player::getForePlayer()
{
    return myfore;
}

Player *Player::getNextPlayer()
{
    return mynext;
}

void Player::storeDispatchCard(const Card& card)
{
    mycards.add(card);
    Cards cards;
    cards.add(card);
    emit noticeGiveCards(this, cards);
}

void Player::storeDispatchCards(const Cards& cards)
{
    mycards.add(cards);
    emit noticeGiveCards(this, cards);
}

Cards Player::getCards()
{
    return mycards;
}

Cards Player::returnCards()
{
    return mycards;
}

void Player::clearCards()
{
    mycards.clear();
}

void Player::playCards(Cards &cards)
{
    mycards.remove(cards);
    emit noticePlayCards(this, cards);
}

void Player::getCards(Cards &cards)
{
    mycards.add(cards);
}

void Player::setCards()
{
    CardList list = mycards.toCardList();
    for (int i = 1, cardcount=0; i < list.size(); i++) {
        Card temp1 = list.at(i);
        Card temp2 = list.at(i-1);
        if(temp1.point()==temp2.point()){
            cardcount+=2;
            if(i<list.size()-1) i--;
            list.remove(i);
            list.remove(i-1);
            mycards.remove(temp1);
            mycards.remove(temp2);
        }
    }
}

void prepareplay(){}
