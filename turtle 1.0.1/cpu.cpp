#include "cpu.h"

#include <QTimer>
#include <QDebug>
#include <QRandomGenerator>

#include "card.h"
#include "player.h"
#include "cardpanel.h"

cpu::cpu(QObject *parent) : QObject(parent) {}

void cpu::setPlayer() {
    // 对象实例化
    myLeftRob = new Player("机器人A", this);
    myRightRob = new Player("机器人B",  this);
    myuser = new Player("我自己",  this);

    myuser->setForePlayer(myLeftRob);
    myuser->setNextPlayer(myRightRob);

    // left robot
    myLeftRob->setForePlayer(myRightRob);
    myLeftRob->setNextPlayer(myuser);

    // right robot
    myRightRob->setForePlayer(myuser);
    myRightRob->setNextPlayer(myLeftRob);

    // 指定当前玩家
    mycurrPlayer = (Player*)myuser;

    // 处理玩家出牌
//    connect(myLeftRob, &Player::noticePlayCards, this, &cpu::disposePlayCards);
//    connect(myRightRob, &Player::noticePlayCards, this, &cpu::disposePlayCards);
//    connect(myuser, &Player::noticePlayCards, this, &cpu::disposePlayCards);
}

Player *cpu::getLeftRobot() { return myLeftRob; }

Player *cpu::getRightRobot() { return myRightRob; }

Player *cpu::getUser() { return myuser; }

void cpu::setCurrentPlayer(Player *player) { mycurrPlayer = player; }

Player *cpu::getCurrentPlayer() { return mycurrPlayer; }

void cpu::setPendPlayer(Player *player) { myPendPlayer = player; }

Player *cpu::getPendPlayer() { return myPendPlayer ; }

void cpu::setAllCards() {
    myallCards.clear();  //清空
    for (int point = Card::Point_3; point <= Card::Point_15; point++)  //每一个点数对应四个花色
    {
        for (int color = Card::Diamond; color <= Card::Spade; color++)  //枚举不能直接算数运算
        {
            Card card((Card::CardPoint)point, (Card::CardColor)color);  //强制转换回枚举类型
            myallCards.add(card);
        }
    }
    for(int i=0; i<17; i++){
        Card temp = myallCards.takeRandomCard();
        myuser->storeDispatchCard(temp);
        myallCards.remove(temp);
    }
    for(int i=0; i<17; i++){
        Card temp = myallCards.takeRandomCard();
        myLeftRob->storeDispatchCard(temp);
        myallCards.remove(temp);
    }
    for(int i=0; i<17; i++){
        Card temp = myallCards.takeRandomCard();
        myRightRob->storeDispatchCard(temp);
        myallCards.remove(temp);
    }
    Card temp = myallCards.takeRandomCard();
    turtle = temp;
}

Card cpu::takeOneCard() {
    return myallCards.takeRandomCard();
}


void cpu::resetCardData() {
    // 洗牌
    setAllCards();
    // 清空所有玩家的牌
    myLeftRob->clearCards();
    myRightRob->clearCards();
    myuser->clearCards();
    // 初始化出牌玩家和牌
    myPendPlayer = nullptr;
    myPendPlayer->clearCards();
}

void cpu::disposePlayCards(Player *player, Cards &cards) {
    Player *fore = player->getForePlayer();
    Player *next = player->getNextPlayer();
    // 1. 将玩家出牌的信号转发给主界面
    emit noticePlayCards(player);
    // 2. 如果不是空牌, 给其他玩家发送信号, 保存出牌玩家对象和打出的牌
    if (!player->returnCards().isEmpty()&&!next->returnCards().isEmpty()) {
        Cards temp;
        int num = QRandomGenerator::global()->bounded(2);
        if(fore->getType()!=next->getType()){
            if(num==0){
                Cards c = fore->returnCards();
                myPendPlayer = fore;
                temp = c.takeRandomCard();
                player->getCards(temp);
                player->setCards();
                fore->playCards(temp);
            }
            if(num==1){
                Cards c = next->returnCards();
                myPendPlayer = next;
                temp = c.takeRandomCard();
                player->getCards(temp);
                player->setCards();
                next->playCards(temp);
            }
            emit sendInfo(player, temp);
        }
        else{
            player->getCards(cards);
            CardPanel* temp;
            temp->setCards(cards);
            temp->setOwner(myPendPlayer);
        }
    }
    // 3. 如果玩家的牌出完了, 计算本局游戏的总分
    if (player->getCards().isEmpty() && !(fore->getCards().isEmpty()&&next->getCards().isEmpty())) {
        next->setForePlayer(fore);
        fore->setNextPlayer(next);
        emit playerStatusChanged(player, cpu::Winning);
        return;
    }
    if(fore->getCards().isEmpty()&&next->getCards().isEmpty()){
        emit playerStatusChanged(player, cpu::Losing);
    }
    // 4. 牌没有出完, 下一个玩家继续出牌
    mycurrPlayer = player->getNextPlayer();
    //mycurrPlayer->prepareplay();
    emit playerStatusChanged(mycurrPlayer, cpu::ThinkingForPlayCards);
}
