#include "cardpanel.h"

#include <QPainter>

CardPanel::CardPanel(QWidget *parent) : QWidget(parent) {}

void CardPanel::setImage(const QPixmap &front, const QPixmap &back) {
    myfront = front;
    myback = back;
    setFixedSize(myfront.size());
    update();
}

QPixmap CardPanel::getImage() {
    return myfront;
}

void CardPanel::setFrontSide(bool flag) {
    isFront = flag;
}

bool CardPanel::isFrontSide() {
    return isFront;
}

void CardPanel::setSeclected(bool flag) {
    isSelect = flag;
}

bool CardPanel::isSelected() {
    return isSelect;
}

void CardPanel::setCards(const Cards &cards) {
    mycards = cards;
}

Cards CardPanel::getCards() {
    return mycards;
}

void CardPanel::setOwner(Player *player) {
    myplayer = player;
}

Player *CardPanel::getOwner() {
    return myplayer;
}

void CardPanel::clicked() {
    emit cardSelected(Qt::LeftButton);
}

void CardPanel::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event)
    QPainter p(this);
    if (isFront) {
        p.drawPixmap(rect(), myfront);
    } else {
        p.drawPixmap(rect(), myback);
    }
}
