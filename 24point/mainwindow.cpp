#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QPainter>
#include <QMessageBox>
#include <QRandomGenerator>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(800, 600);
    setCards();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getPlayerName(QString name)
{
    PlayerName = name;
}

void MainWindow::setCards()
{
    int num[4];
    for(int i=0; i<4; i++){
        num[i] = QRandomGenerator::global()->bounded(15) + 1;
    }
    for(int i=3; i>=1; i--){
        for(int j=1; j<=i; j++){
            if(num[j-1]>num[j]){
                int temp = num[j-1];
                num[j-1] = num[j];
                num[j] = temp;
            }
        }
    }
    for(int i=0; i<4; i++){
        point[i] = num[i];
    }
    qDebug() << point[0] << point[1] << point[2] << point[3];
}

double MainWindow::cal(double x, double y, int n)
{
    if(n==1) return x+y;
    if(n==2) return x-y;
    if(n==3) return x*y;
    if(n==4) return x/y;
    if(n==5) return pow(x, y);
    if(n==6) return pow(x, -y);
    return 0;
}

bool MainWindow::judge(double a[], int n)
{
    if(n==1) return fabs(a[0]-24)<0.00000001;
    if(n==2){
        for(int i=1;i<=6;i++){
            double b[1];
            b[0] = cal(a[0],a[1],i);
            return judge(b,1);
        }
    }
    if(n==3){
        for(int i=1;i<=6;i++){
            double b[2];
            b[1] = cal(a[1],a[2],i);
            return judge(b,2);
        }
    }
    if(n==4){
        for(int i=1;i<=6;i++){
            double b[3];
            b[2] = cal(a[2],a[3],i);
            return judge(b,3);
        }
    }
    return 0;
}

bool MainWindow::judgeAll(double a[4])
{
    int m[6][4]={0,1,2,3, 0,1,3,2, 0,2,1,3, 0,2,3,1, 0,3,1,2, 0,3,2,1};
    int sum=0;
    double t[24][4];
    for(int i=0;i<24;i++){
        for(int j=0;j<4;j++){
            t[i][j]=a[i/6+m[i%6][j]];
        }
        sum+=judge(t[i],4);
    }
    return sum;
}

QString MainWindow::solve(double *a, int n)
{
    QString p;
    QString blank = " ";
    if(n==1){
        if(fabs(a[0]-24)<0.00000001) return p;
        else return blank;
    }
    if(n==2){
        for(int i=1;i<=6;i++){
            double b[1];
            b[0] = cal(a[0],a[1],i);
            return p;
        }
    }
    if(n==3){
        for(int i=1;i<=6;i++){
            double b[2];
            b[1] = cal(a[1],a[2],i);
            return solve(b,3);
        }
    }
    if(n==4){
        for(int i=1;i<=6;i++){
            double b[3];
            b[2] = cal(a[2],a[3],i);
            return solve(b,3);
        }
    }
    return blank;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    for(int i=0; i<4; i++){
        p.setRenderHint(QPainter::Antialiasing);
        p.drawPixmap(100+150*i, 150, 150, 250,"://picture/" + tr("%1").arg(point[i]) + ".png");
    }
}

void MainWindow::on_endButton_clicked()
{
    if (QMessageBox::Close  ==  QMessageBox::information(this,"","是否确认退出游戏？",QMessageBox::Close|QMessageBox::Cancel,QMessageBox::Cancel)){
        QMessageBox::information(this,"BYE BYE！","下次再一起玩吧！");
        close();
        qDebug() << "退出";
    }
    else{
        qDebug() << "取消";
    }
}

void MainWindow::on_renewButton_clicked()
{
    if (QMessageBox::Yes  ==  QMessageBox::information(this,"","是否刷新卡牌？",QMessageBox::Yes|QMessageBox::Cancel,QMessageBox::Cancel)){
        setCards();
        update();
        qDebug() << "刷新";
    }
    else{
        qDebug() << "取消";
    }
}

void MainWindow::on_tipButton_clicked()
{
    QMessageBox box;
    box.setStandardButtons(QMessageBox::Close);
    box.setText("是否能通过“+-×÷”用已知的四个数得到24呢？");
    box.exec();
}

void MainWindow::on_yesButton_clicked()
{
    double a[4]={(double)point[0], (double)point[1], (double)point[2], (double)point[3]};
    if(judgeAll(a)) QMessageBox::information(this,"大拇哥","恭喜你回答正确，确实可以");
    else QMessageBox::critical(this,"寄咯","答案是不能，别伤心，再来！");
}

void MainWindow::on_noButton_clicked()
{
    double a[4]={(double)point[0], (double)point[1], (double)point[2], (double)point[3]};
    if(!judgeAll(a)) QMessageBox::information(this,"大拇哥","恭喜你回答正确，确实不行");
    else QMessageBox::critical(this,"寄咯","答案是可以，要不再想想？！");
}

static int n=6;
void MainWindow::on_ansButton_clicked()
{
    QMessageBox box;
    box.setFixedSize(200, 80);
    if(n==5){
        box.setWindowTitle("zhengzaishuijiaoderen");
        box.setText("别叫我，我还要睡！！！");
        box.exec();
    }
    if(n==3){
        box.setWindowTitle("haizaishuijiaoderen");
        box.setText("别烦我，自己玩去！！！");
        box.exec();
    }
    if(n==1){
        box.exec();
    }
    if(n==0){
        double a[4]={(double)point[0], (double)point[1], (double)point[2], (double)point[3]};
        if(!judgeAll(a)){
            box.setWindowTitle("一种可能的解法");
            box.setText(solution);
        }
        else{
            box.setWindowTitle("凑不出来");
            box.setText("按钮被题目揍得重伤倒地");
        }
        box.exec();
        n=7;
    }
    n--;
}
