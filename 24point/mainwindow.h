#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void getPlayerName(QString name);
    void setCards();
    void setScene();

    double cal(double x, double y, int n);
    bool judge(double a[], int n);
    bool judgeAll(double a[4]);
    QString solve(double a[], int n);

    void paintEvent(QPaintEvent *);


private slots:
    void on_endButton_clicked();

    void on_renewButton_clicked();

    void on_tipButton_clicked();

    void on_yesButton_clicked();

    void on_noButton_clicked();

    void on_ansButton_clicked();

private:
    Ui::MainWindow *ui;
    int point[4];
    QString PlayerName;
    QPixmap pic;

    QString solution;

};
#endif // MAINWINDOW_H
