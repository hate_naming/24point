#ifndef CARD_H
#define CARD_H

#include <QVector>
#include <QWidget>
#include <QSet>
#include "player.h"

class Card {
public:
    enum CardColor {
        Diamond,
        Club,
        Heart,
        Spade
    };
    enum CardPoint {
        Point_3,
        Point_4,
        Point_5,
        Point_6,
        Point_7,
        Point_8,
        Point_9,
        Point_10,
        Point_11,  //J
        Point_12,  //Q
        Point_13,  //K
        Point_14,  //A
        Point_15,  //2
        Point_16,  // smalljoker 小王
        Point_17   // bigjoker 大王
    };

    Card();
    Card(CardPoint point, CardColor color);

    void setPoint(CardPoint point);
    void setColor(CardColor color);
    CardPoint point() const;
    CardColor color() const;

private:
    CardPoint mypoint;
    CardColor mycolor;
};

// 对象比较
bool lessSort(const Card& c1, const Card& c2);
bool greaterSort(const Card& c1, const Card& c2);
bool operator<(const Card& c1, const Card& c2);

// 操作符重载 (==) 用于Qset存储
bool operator==(const Card& left, const Card& right);

// 重写全局函数 qHash 用于Qset 存储
uint qHash(const Card& card);

// 定义类型的别名
using CardList = QVector<Card>;


class Cards {
public:
    enum ColorType { Asc, Desc, NoSort };  //枚举 排序方式
    Cards();
    explicit Cards(const Card& card);

    /*参数同一使用常引用*/
    // 添加扑克牌，发牌
    void add(const Card& card);             //添加一张
    void add(const Cards& cards);           //添加多张
    void add(const QVector<Cards>& cards);  //添加

    // 一次性插入多个数据 (操作符重载 << )，单纯重载操作符，返回引用，可链式操作
    Cards& operator<<(const Card& card);
    Cards& operator<<(const Cards& cards);

    // 删除扑克牌，出牌
    void remove(const Card& card);    //删除一张
    void remove(const Cards& cards);  //删除多张
    void remove(const QVector<Cards>& cards);

    // 扑克牌的数量
    int cardCount();
    // 是否为空
    bool isEmpty();
    void clear();

    // 最大点数
    Card::CardPoint maxPoint();
    // 最小点数
    Card::CardPoint minPoint();
    // 指定点数的牌的数量
    int pointCount(Card::CardPoint point);
    // 某张牌是否在集合中
    bool contains(const Card& card);
    bool contains(const Cards& cards);

    // 随机取出一张扑克牌
    Card takeRandomCard();

    // QVector<Card>
    // QSet -> QVector
    CardList toCardList(ColorType type = Desc);

    // 测试函数, 打印所有的卡牌信息
    void printAllCardInfo();

private:
    QSet<Card> mycards;  // Qset不能自动排序，需要自己实现；STL 中的 set 可以自动排序
};

class CardPanel : public QWidget {
    Q_OBJECT
public:
    explicit CardPanel(QWidget* parent = nullptr);

    // 设置获取图片函数
    void setImage(const QPixmap& front, const QPixmap& back);
    QPixmap getImage();  //获取当前显示图片

    // 设置扑克牌显示面
    void setFrontSide(bool flag);
    bool isFrontSide();

    // 记录窗口是否被选中了
    void setSeclected(bool flag);
    bool isSelected();

    // 扑克牌的花色以及点数
    void setCard(const Card& card);
    Card getCard();  //获取card实例

    // 扑克牌的所有者
    void setOwner(Player* player);
    Player* getOwner();

    // 模拟扑克牌的点击事件
    void clicked();

protected:
    void paintEvent(QPaintEvent* event);  //画图事件来显示图片 由update()触发
    void mousePressEvent(QMouseEvent* event);  //

signals:
    void cardSelected(Qt::MouseButton button);

private:
    QPixmap myfront;        //正面
    QPixmap myback;         //反面
    bool isfront = true;  //当前显示面
    bool isSelect = false;
    Card mycard;
    Player* owner = nullptr;  //所有者
};

#endif  // CARD_H
