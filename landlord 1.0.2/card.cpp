#include "card.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QPainter>
#include <QMouseEvent>

Card::Card() {}

Card::Card(CardPoint point, CardColor color) {
    setPoint(point);
    setColor(color);
}

void Card::setPoint(CardPoint point) { mypoint = point; }

void Card::setColor(CardColor color) { mycolor = color; }

Card::CardPoint Card::point() const { return mypoint; }

Card::CardColor Card::color() const { return mycolor; }

bool lessSort(const Card &c1, const Card &c2)  //由大到小排序
{
    if (c1.point() == c2.point())  //点数相同，判断花色
    {
        return c1.color() < c2.color();
    } else {
        return c1.point() < c2.point();
    }
}

bool greaterSort(const Card &c1, const Card &c2) {
    if (c1.point() == c2.point()) {
        return c1.color() > c2.color();
    } else {
        return c1.point() > c2.point();
    }
}

bool operator==(const Card &left, const Card &right) {
    return (left.point() == right.point() && left.color() == right.color());
}
//确保 唯一 对 唯一 输出即可
uint qHash(const Card &card) { return card.point() * 100 + card.color(); }

bool operator<(const Card &c1, const Card &c2) { return lessSort(c1, c2); }



Cards::Cards() {}

Cards::Cards(const Card &card) { add(card); }

void Cards::add(const Card &card) { mycards.insert(card); }

void Cards::add(const Cards &cards) {
    mycards.unite(cards.mycards);  //合并Qset操作，存储到调用者中
}

void Cards::add(const QVector<Cards> &cards) {
    for (int i = 0; i < cards.count(); i++) {
        add(cards.at(i));
    }
}

Cards &Cards::operator<<(const Card &card) {
    add(card);
    return *this;
}

Cards &Cards::operator<<(const Cards &cards) {
    add(cards);
    return *this;
}

void Cards::remove(const Card &card) { mycards.remove(card); }

void Cards::remove(const Cards &cards) {
    mycards.subtract(cards.mycards);  //差集Qset操作，对原Qset 删除相同的元素
}

void Cards::remove(const QVector<Cards> &cards) {
    for (int i = 0; i < cards.size(); i++) {
        remove(cards.at(i));
    }
}

int Cards::cardCount() { return mycards.size(); }

bool Cards::isEmpty() { return mycards.isEmpty(); }

void Cards::clear() { mycards.clear(); }

Card::CardPoint Cards::maxPoint() {
    Card::CardPoint max =
            Card::Point_3;  //类内定义的枚举类型可以使用“静态”的方法引用
    if (!mycards.isEmpty()) {
        for (auto temp = mycards.begin(); temp != mycards.end(); temp++) {
            if (temp->point() > max) {
                max = temp->point();  //获取最大值
            }
        }
    }
    return max;  //容器为空，返回Card_Begin
}

Card::CardPoint Cards::minPoint() {
    Card::CardPoint min = Card::Point_17;
    if (!mycards.isEmpty()) {
        for (auto temp = mycards.begin(); temp != mycards.end(); temp++) {
            if (temp->point() < min) {
                min = temp->point();  //获取最小值
            }
        }
    }
    return min;  //容器为空 返回Card_End
}

int Cards::pointCount(Card::CardPoint point)  //某一点数的个数
{
    int count = 0;
    for (auto temp = mycards.begin(); temp != mycards.end(); temp++) {
        if (temp->point() == point) {
            count++;
        }
    }
    return count;
}

bool Cards::contains(const Card &card) { return mycards.contains(card); }

bool Cards::contains(const Cards &cards) {
    return mycards.contains(cards.mycards);
}

Card Cards::takeRandomCard() {
    // 生成一个随机数
    int num = QRandomGenerator::global()->bounded(
                mycards.size());  //随机数0~max-1，左闭右开，可使用double
    QSet<Card>::const_iterator temp = mycards.constBegin();  //只读迭代器
    for (int i = 0; i < num; i++, temp++)  //循环递增到指定的位置 后移num
        ;
    Card card = *temp;    //获取迭代器指向的牌的拷贝
    mycards.erase(temp);  //删除该牌
    return card;        //返回拷贝
}

CardList Cards::toCardList(ColorType type) {
    CardList list;
    for (auto temp = mycards.begin(); temp != mycards.end(); temp++) {
        list << *temp;  //传入list
    }
    if (type == Asc) {  //升序
        //参数3 是一个返回类型为bool的回调函数 ，指定排序方式，左右符合
        //不换(true)，左右不符 换(false)
        std::sort(list.begin(), list.end(), lessSort);
    } else if (type == Desc) {  //降序
        std::sort(list.begin(), list.end(), greaterSort);
    }
    return list;
}

void Cards::printAllCardInfo() {
    QString text;
    char p[] = "JQKA2";
    for (auto temp = mycards.begin(); temp != mycards.end(); temp++) {
        QString msg;
        Card::CardPoint point = temp->point();
        Card::CardColor color = temp->color();
        if (color == Card::CardColor::Club) { msg = "梅花"; }
        if (color == Card::CardColor::Diamond) { msg = "方片"; }
        if (color == Card::CardColor::Heart) { msg = "红桃"; }
        if (color == Card::CardColor::Spade){ msg = "黑桃"; }
        if (point >= Card::Point_3 && point <= Card::Point_10) {
            msg = QString("%1%2").arg(msg).arg(point + 2);
        } else if (point >= Card::Point_11 && point <= Card::Point_15) {
            msg = QString("%1%2").arg(msg).arg(p[point - Card::Point_11]);
        }
        if (point == Card::Point_16) {
            msg = "大王";
        }
        if (point == Card::Point_17) {
            msg = "小王";
        }
        msg += "  ";
        text += msg;
    }
    qDebug() << text;
}
CardPanel::CardPanel(QWidget *parent) : QWidget(parent) {}

void CardPanel::setImage(const QPixmap &front, const QPixmap &back) {
  myfront = front;
  myback = back;

  setFixedSize(myfront.size());

  update();
}

QPixmap CardPanel::getImage() { return myfront; }

void CardPanel::setFrontSide(bool flag) { isfront = flag; }

bool CardPanel::isFrontSide() { return isfront; }

void CardPanel::setSeclected(bool flag) { isSelect = flag; }

bool CardPanel::isSelected() { return isSelect; }

void CardPanel::setCard(const Card &card) { mycard = card; }

Card CardPanel::getCard() { return mycard; }

void CardPanel::setOwner(Player *player) { owner = player; }

Player *CardPanel::getOwner() { return owner; }

void CardPanel::clicked() { emit cardSelected(Qt::LeftButton); }

void CardPanel::paintEvent(QPaintEvent *event) {
  Q_UNUSED(event)
  QPainter p(this);
  if (isfront) {
    p.drawPixmap(rect(), myfront);
  } else {
    p.drawPixmap(rect(), myback);
  }
}

void CardPanel::mousePressEvent(QMouseEvent *event) {
  emit cardSelected(event->button());
}
