#include "gamescene.h"
#include "loading.h"

#include <QPainter>
#include <QTimer>

Loading::Loading(QWidget *parent) : QWidget(parent)
{
    background.load(":/image/loading.png");
    setFixedSize(background.size());

    // 去边框
    setWindowFlags(Qt::FramelessWindowHint | windowFlags());
    // 背景透明
    setAttribute(Qt::WA_TranslucentBackground);

    QPixmap pixmap(":/image/progress.png");
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [=](){
        progress = pixmap.copy(0, 0, dist, pixmap.height());
        update();
        if(dist >= pixmap.width())
        {
            timer->stop();
            timer->deleteLater();
            GameScene* scene = new GameScene;
            scene->show();
            close();
        }
        dist += 3;
    });
    timer->start(15);
}

void Loading::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    p.drawPixmap(rect(), background);
    p.drawPixmap(62, 417, progress.width(), progress.height(), progress);
}
