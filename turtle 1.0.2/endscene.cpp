#include "endscene.h"

#include <QPainter>

#include "gamescene.h"

EndScene::EndScene(QWidget *parent) : QMainWindow(parent){}

EndScene::EndScene(bool isWin, bool isFalse, QWidget *parent) : QMainWindow(parent)
{
    myText = new QLabel(this);
    if(isWin==1)
    {
        myText->setText("恭喜你获胜！");
        myText->move(125, 125);
    }
    else if(isFalse==1)
    {
        myText->setPixmap(QPixmap(":/images/lose.jpg"));
        QLabel *text = new QLabel(this);
        text->setText("不要气馁喔！");
        text->setSizeIncrement(170,25);
        text->move(125,285);
        myText->move(125, 125);
    }
    // 继续游戏按钮
    mycontinue = new QPushButton(this);
    mycontinue->move(84, 405);
    mycontinue->setFixedSize(231, 48);
    connect(mycontinue, SIGNAL(&QPushButton::clicked), this, SLOT([=](){
        this->close();
        GameScene *w = new GameScene;
        w->show();
    }));

    //结束游戏按钮
    myend = new QPushButton(this);
    myend->move(84, 453);
    myend->setFixedSize(231, 48);
    connect(myend, SIGNAL(&QPushButton::clicked), this, SLOT(close()));
}

void EndScene::paintEvent(QPaintEvent *ev)
{
    Q_UNUSED(ev)
    QPainter p(this);
    //p.drawPixmap(rect(), background);
}
