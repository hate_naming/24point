#ifndef CPU_H
#define CPU_H

#include <QTimer>
#include <QDebug>
#include <QObject>
#include <QRandomGenerator>

#include "card.h"
#include "player.h"


//游戏控制逻辑
class cpu : public QObject {
    Q_OBJECT
public:
    // 游戏状态
    enum GameStatus { DispatchCard, Fighting };
    // 玩家状态
    enum PlayerStatus { Playing, End };

    explicit cpu(QObject* parent = nullptr);

    // 初始化玩家
    void setPlayer();

    // 得到玩家的实例对象
    Player* getLeftRobot();
    Player* getRightRobot();
    Player* getUser();

    // 当前玩家
    void setCurrentPlayer(Player* player);
    Player* getCurrentPlayer();
    // 取出出牌玩家的对象
    void setPendPlayer(Player* player);
    Player* getPendPlayer();
    Cards getPendCards();

    // 初始化扑克牌
    void setAllCards();
    // 每次发一张牌
    Card takeOneCard();
    // 得到乌龟底牌
    Cards getcpuCards();
    // 重置卡牌数据
    void resetCardData();
    // 处理出牌
    void disposePlayCards(Player* player, Cards& cards);

signals:
    // 玩家状态变化
    void playerStatusChanged(Player* player, PlayerStatus status);
    // 游戏状态变化
    void gameStatusChanged(GameStatus status);
    // 通知玩家回合结束
    void noticePlayCards(Player* player, Cards& cards);
    // 传递玩家回合数据
    void sendInfo(Player* player, Cards& cards);

private:
    Player* myLeftRob = nullptr;
    Player* myRightRob = nullptr;
    Player* myuser = nullptr;
    Player* mycurrPlayer = myuser;  //当前玩家
    Player* myPendPlayer = nullptr;
    Cards myallCards;
    Cards mypendCards;
};



#endif // CPU_H
