#ifndef CARDPANEL_H
#define CARDPANEL_H

#include <QWidget>

#include "card.h"
#include "cards.h"
#include "player.h"

class CardPanel : public QWidget {
    Q_OBJECT
public:
    explicit CardPanel(QWidget* parent = nullptr);

    // 设置获取图片函数
    void setImage(const QPixmap& front, const QPixmap& back);
    QPixmap getImage();  //获取当前显示图片

    // 设置扑克牌显示面
    void setFrontSide(bool flag);
    bool isFrontSide();

    // 记录窗口是否被选中了
    void setSeclected(bool flag);
    bool isSelected();

    // 扑克牌的花色以及点数
    void setCards(const Cards& cards);
    Cards getCards();  //获取card实例

    // 扑克牌的所有者
    void setOwner(Player* player);
    Player* getOwner();

    // 模拟扑克牌的点击事件
    void clicked();

protected:
    void paintEvent(QPaintEvent* event);  //画图事件来显示图片 由update()触发

signals:
    void cardSelected(Qt::MouseButton button);

private:
    QPixmap myfront;        //正面
    QPixmap myback;         //反面
    bool isFront = 1;  //当前显示面
    bool isSelect = 0;
    Cards mycards;
    Player* myplayer = nullptr;  //所有者
};

#endif // CARDPANEL_H
