#include "login.h"
#include "ui_login.h"
#include "loading.h"
#include <QMessageBox>
#include <QDebug>

Login::Login(QWidget *parent): QMainWindow(parent), ui(new Ui::login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

QString Login::getPlayerName()
{
    return PlayerName;
}

static int n=3;

void Login::on_pushButton_clicked()
{
    qDebug() << "点击按钮";
   if(n-->=1||ui->lineEdit->text()!="\0")
   {
       if(ui->lineEdit->text()!="\0")
       {
           PlayerName = ui->lineEdit->text();
           this->close();
           Loading *loading = new Loading;
           loading->show();
       }
       else
       {
           QMessageBox::information(this,"提示","请输入您的名字!");
       }
   }
   else
   {
       QMessageBox::critical(this,"BYE BYE！","下次再一起玩吧！");
       this->close();
   }
}

void Login::on_lineEdit_returnPressed()
{
    qDebug() << "按下回车";
    if(n-->=1||ui->lineEdit->text()!="\0")
    {
        if(ui->lineEdit->text()!="\0")
        {
            PlayerName = ui->lineEdit->text();
            this->close();
            Loading *loading = new Loading;
            loading->show();
        }
        else
        {
            QMessageBox::information(this,"提示","请输入您的名字!");
        }
    }
    else
    {
        QMessageBox::critical(this,"BYE BYE！","下次再一起玩吧！");
        this->close();
    }
}
