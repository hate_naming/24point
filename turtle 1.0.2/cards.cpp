#include "cards.h"

#include <QDebug>
#include <QRandomGenerator>

Cards::Cards() {}

Cards::Cards(const Card &card) { add(card); }

void Cards::add(const Card &card) { mycardset.insert(card); }

void Cards::add(const Cards &cards) {
    mycardset.unite(cards.mycardset);  //合并Qset操作，存储到调用者中
}

void Cards::add(const QVector<Cards> &cards) {
    qDebug() << cards.count();
    for (int i = 0; i < cards.count(); i++) {
        add(cards.at(i));
    }
}

Cards &Cards::operator<<(const Card &card) {
    add(card);
    return *this;
}

Cards &Cards::operator<<(const Cards &cards) {
    add(cards);
    return *this;
}

void Cards::remove(const Card &card) { mycardset.remove(card); }

void Cards::remove(const Cards &cards) {
    mycardset.subtract(cards.mycardset);  //差集Qset操作，对原Qset 删除相同的元素
}

void Cards::remove(const QVector<Cards> &cards) {
    qDebug() << cards.size();
    for (int i = 0; i < cards.size(); i++) {
        remove(cards.at(i));
    }
}

int Cards::cardCount() { return mycardset.size(); }

bool Cards::isEmpty() { return mycardset.isEmpty(); }

void Cards::clear() { mycardset.clear(); }

Card::CardPoint Cards::maxPoint() {
    Card::CardPoint max = Card::Point_3;
    if (!mycardset.isEmpty()) {
        for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
            if (temp->point() > max) {
                max = temp->point();
            }
        }
    }
    return max;
}

Card::CardPoint Cards::minPoint() {
    Card::CardPoint min = Card::Point_15;
    if (!mycardset.isEmpty()) {
        for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
            if (temp->point() < min) {
                min = temp->point();
            }
        }
    }
    return min;
}

int Cards::pointCount(Card::CardPoint point)  //某一点数的个数
{
    int count = 0;
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        if (temp->point() == point) {
            count++;
        }
    }
    return count;
}

Card Cards::takeRandomCard() {
    int num = QRandomGenerator::global()->bounded(mycardset.size());  //随机数0~max-1，左闭右开，可使用double
    QSet<Card>::const_iterator temp = mycardset.constBegin();  //只读迭代器
    for (int i = 0; i < num; i++, temp++);  //循环递增到指定的位置 后移num
    Card card = *temp;    //获取迭代器指向的牌的拷贝
    mycardset.erase(temp);  //删除该牌
    return card;        //返回拷贝
}

CardList Cards::toCardList(SetOrder type) {
    CardList list;
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        list << *temp;  //传入list
    }
    if (type == Asc) {  //升序
        std::sort(list.begin(), list.end(), smallPoint);
        int j=0;
        for(auto i=list.begin() + 1; i<list.end(); i++, j++){
            Card *temp1=i;
            Card *temp2=i-1;
            if(temp1->point()==temp2->point()){
                list.remove(j);
                list.remove(j-1);
                j-=2;
                i-=2;
            }
        }
    } else if (type == Desc) {  //降序
        std::sort(list.begin(), list.end(), bigPoint);
        int j=0;
        for(auto i=list.begin() + 1; i<list.end(); i++, j++){
            Card *temp1=i;
            Card *temp2=i-1;
            if(temp1->point()==temp2->point()){
                list.remove(j);
                list.remove(j-1);
                j-=2;
                i-=2;
            }
        }
    }
    return list;
}

void Cards::printCardDebug() {
    QString text;
    char p[] = "JQKA2";
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        QString msg;
        Card::CardPoint point = temp->point();
        Card::CardColor color = temp->color();
        if (color == Card::CardColor::Club) { msg = "梅花"; }
        if (color == Card::CardColor::Diamond) { msg = "方片"; }
        if (color == Card::CardColor::Heart) { msg = "红桃"; }
        if (color == Card::CardColor::Spade){ msg = "黑桃"; }
        if (point >= Card::Point_3 && point <= Card::Point_10) {
            msg = QString("%1%2").arg(msg).arg(point + 2);
        } else if (point >= Card::Point_11 && point <= Card::Point_15) {
            msg = QString("%1%2").arg(msg).arg(p[point - Card::Point_11]);
        }
        msg += "  ";
        text += msg;
    }
    qDebug() << text;
}
