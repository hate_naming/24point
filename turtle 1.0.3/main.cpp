#include "login.h"
#include "gamescene.h"

#include <QResource>
#include <QApplication>

#include "loading.h"
//#include "gamescene.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<Cards>("Cards&");
    qRegisterMetaType<Cards>("Cards");
    //QResource::registerResource("./resource.rcc");
    Login w;
    w.show();
    QString name = w.getPlayerName();
    return a.exec();
}
