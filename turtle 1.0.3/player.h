#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QThread>

#include "card.h"
#include "cards.h"

class Player : public QObject
{
    Q_OBJECT
public:
    enum Type { Robot, User, Unknown };  // 玩家类型

    Player(QObject *parent);
    Player(QString name, QObject* parent = nullptr);
    Player(QString name, Player::Type playertype, QObject* parent = nullptr);

    // 名字
    void setName(QString name);
    QString getName();

    // 玩家类型
    void setType(Type type);
    Type getType();

    // 游戏结果
    void setWin(bool flag);
    bool isWin();
    void setLose(bool flag);
    bool isLose();

    // 提供当前对象的上家/下家对象
    void setForePlayer(Player* player);
    void setNextPlayer(Player* player);
    Player* getForePlayer();
    Player* getNextPlayer();    

    // 存储扑克牌(发牌的时候得到的)
    void storeDispatchCard(const Card& card);
    void storeDispatchCards(const Cards& cards);

    // 得到所有的牌
    Cards getCards();
    // 清空玩家手中所有的牌
    void clearCards();

    // 出牌
    void playCards(Cards &cards);
//    void getCards(Cards &cards);
//    void setCards();

//    // 获取待出牌玩家对象以及这个玩家打出的牌
//    Player* getPendPlayer();
//    Cards getPendCards();

//    // 存储出牌玩家对象和打出的牌
//    void storePend(Player* player, const Cards& cards);

signals:
    // 通知已经发牌了
    void noticeGetCards(Player* player, const Cards& cards);
    // 通知已经出牌
    void noticePlayCards(Player* player, Cards& cards);

private:
    QString myname;
    Type mytype;
    bool winFlag = false;
    bool loseFlag = false;
    Player* myfore = nullptr;  //上家
    Player* mynext = nullptr;  //下家
    Cards mycards;             // 玩家手牌
//    Cards mypendCards;
//    Player* mypendPlayer = nullptr; //上一次出牌玩家
};

#endif // PLAYER_H
