#include "endscene.h"

#include <QPainter>

EndScene::EndScene(QWidget *parent) : QMainWindow(parent){}

EndScene::EndScene(int flag, QWidget *parent) : QMainWindow(parent)
{
    setFixedSize(400, 500);
    myText = new QLabel(this);
    if(flag==666)
    {
        myText->setText("恭喜你获胜！");
        myText->move(125, 125);
    }
    else if(flag==886)
    {
        myText->setPixmap(QPixmap(":/images/lose.jpg"));
        QLabel *text = new QLabel(this);
        text->setText("不要气馁喔！");
        text->setSizeIncrement(170,25);
        text->move(125,285);
        myText->move(125, 125);
    }

    // 继续游戏按钮
    mycontinue = new QPushButton(this);
    mycontinue->move(84, 409);
    mycontinue->setFixedSize(231, 48);
    connect(mycontinue, &QPushButton::clicked, this, &EndScene::continueGame);

    //结束游戏按钮
    myend = new QPushButton(this);
    myend->move(84, 453);
    myend->setFixedSize(231, 48);
    connect(myend, &QPushButton::clicked, this, &EndScene::endGame);
}

EndScene::~EndScene(){}

void EndScene::paintEvent(QPaintEvent *ev)
{
    Q_UNUSED(ev)
    QPainter p(this);
    //p.drawPixmap(rect(), background);
}
