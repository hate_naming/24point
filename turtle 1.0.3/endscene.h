#ifndef ENDSCENE_H
#define ENDSCENE_H

#include <QLabel>
#include <QMainWindow>
#include <QPushButton>

#include "player.h"

class EndScene : public QMainWindow
{
    Q_OBJECT
public:
    EndScene();
    EndScene(QWidget *parent = nullptr);
    EndScene(int flag, QWidget *parent = nullptr);
    ~EndScene();
signals:
    void endGame();
    void continueGame();

protected:
    void paintEvent(QPaintEvent* ev);

private:
    QPixmap background;
    QLabel* myText;
    QPushButton* mycontinue;
    QPushButton* myend;
};

#endif // ENDSCENE_H
