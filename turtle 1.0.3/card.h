#ifndef CARD_H
#define CARD_H

#include <QSet>
#include <QVector>
#include <QPixmap>

class Card {
public:
    enum CardColor {
        Diamond,
        Club,
        Heart,
        Spade
    };
    enum CardPoint {
        Point_3,
        Point_4,
        Point_5,
        Point_6,
        Point_7,
        Point_8,
        Point_9,
        Point_10,
        Point_11,  //J
        Point_12,  //Q
        Point_13,  //K
        Point_14,  //A
        Point_15,  //2
    };
    Card();
    Card(CardPoint point, CardColor color);
    void setPoint(CardPoint point);
    void setColor(CardColor color);
    CardPoint point() const;
    CardColor color() const;

private:
    CardPoint mypoint;
    CardColor mycolor;
};

// 对象比较
bool smallPoint(const Card& c1, const Card& c2);
bool bigPoint(const Card& c1, const Card& c2);
bool operator<(const Card& c1, const Card& c2);
bool operator>(const Card& c1, const Card& c2);

// 操作符重载 (==) 用于Qset存储
bool operator==(const Card& left, const Card& right);

// 重写全局函数 qHash 用于Qset 存储
uint qHash(const Card& card);

// 定义类型的别名
using CardList = QVector<Card>;

#endif  // CARD_H
