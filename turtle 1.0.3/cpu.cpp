#include "cpu.h"

#include <QTimer>
#include <QDebug>
#include <QRandomGenerator>

#include "card.h"
#include "player.h"
#include "cardpanel.h"

cpu::cpu(QObject *parent) : QObject(parent) {}

void cpu::setPlayer() {
    // 对象实例化
    myLeftRob = new Player("机器人A", this);
    myRightRob = new Player("机器人B",  this);
    myuser = new Player("我自己",  this);

    myuser->setForePlayer(myLeftRob);
    myuser->setNextPlayer(myRightRob);

    // left robot
    myLeftRob->setForePlayer(myRightRob);
    myLeftRob->setNextPlayer(myuser);

    // right robot
    myRightRob->setForePlayer(myuser);
    myRightRob->setNextPlayer(myLeftRob);

    // 指定当前玩家
    mycurrPlayer = (Player*)myuser;

    // 传递出牌玩家对象和玩家打出的牌
    connect(this, SIGNAL(&cpu::sendInfo), myLeftRob, SLOT(&Player::storePend));
    connect(this, SIGNAL(&cpu::sendInfo), myRightRob, SLOT(&Player::storePend));
    connect(this, SIGNAL(&cpu::sendInfo), myuser, SLOT(&Player::storePend));

    // 处理玩家出牌
    connect(myLeftRob, SIGNAL(&Player::noticePlayCards), this, SLOT(&cpu::disposePlayCards));
    connect(myRightRob, SIGNAL(&Player::noticePlayCards), this, SLOT(&cpu::disposePlayCards));
    connect(myuser, SIGNAL(&Player::noticePlayCards), this, SLOT(&cpu::disposePlayCards));
}

Player *cpu::getLeftRobot() { return myLeftRob; }

Player *cpu::getRightRobot() { return myRightRob; }

Player *cpu::getUser() { return myuser; }

void cpu::setCurrentPlayer(Player *player) { mycurrPlayer = player; }

Player *cpu::getCurrentPlayer() { return mycurrPlayer; }

//void cpu::setPendPlayer(Player *player) { myPendPlayer = player; }

//Player *cpu::getPendPlayer() { return myPendPlayer ; }

//Cards cpu::getPendCards() { return mypendCards; }

void cpu::setAllCards() {
    myallCards.clear();  //清空
    for (int point = Card::Point_3; point <= Card::Point_15; point++)  //每一个点数对应四个花色
    {
        for (int color = Card::Diamond; color <= Card::Spade; color++)  //枚举不能直接算数运算
        {
            Card card((Card::CardPoint)point, (Card::CardColor)color);  //强制转换回枚举类型
            myallCards.add(card);
        }
    }
}

Card cpu::takeOneCard() {
    return myallCards.takeRandomCard();
}

Cards cpu::getcpuCards() { return myallCards; }

void cpu::resetCardData() {
    setAllCards();
    myLeftRob->clearCards();
    myRightRob->clearCards();
    myuser->clearCards();
//    myPendPlayer = nullptr;
//    mypendCards.clear();
//    myPendPlayer->clearCards();
}

void cpu::disposePlayCards(Player *player, Cards &cards) {
    emit noticePlayCards(player, cards);  // 1. 将玩家出牌的信号转发给主界面
    Player *fore = player->getForePlayer();
    Player *next = player->getNextPlayer();

    // 2. 如果不是空牌, 给其他玩家发送信号, 保存出牌玩家对象和打出的牌
    if (!player->getCards().isEmpty()&&!next->getCards().isEmpty()) {
        emit playerStatusChanged(player, cpu::Playing);
        return;
    }
    // 3. 如果玩家的牌出完了, 计算本局游戏的总分
    if (player->getCards().isEmpty()) {
        if(player == myuser){
            player->setWin(1);
            emit playerStatusChanged(player, cpu::End);
            return;
        }
        if(next == myuser){
            player->setLose(1);
            emit playerStatusChanged(player, cpu::End);
        }
        fore->setNextPlayer(next);
        next->setForePlayer(fore);
        emit playerStatusChanged(player, cpu::Playing);
        return;
    }
    // 4. 牌没有出完, 下一个玩家继续出牌
    mycurrPlayer = player->getNextPlayer();
    emit playerStatusChanged(mycurrPlayer, cpu::Playing);
}
