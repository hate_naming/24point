#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QMap>
#include <QLabel>
#include <QPushButton>
#include <QMainWindow>

#include "cpu.h"
#include "player.h"
#include "cardpanel.h"

class GameScene : public QMainWindow
{
    Q_OBJECT

public:
    GameScene(QWidget *parent = nullptr);
    ~GameScene();
    // 初始化游戏控制类信息
    void setcpu();
    // 切割并存储图片
    void setCardMap();
    // 加载卡牌图片信息
    void cropImage(QPixmap& pix, Card card);
    // 初始化玩家在窗口中的上下文环境
    void setPlayerContext();
    // 初始化游戏场景
    void setGameScene();
    // 处理游戏的状态
    void gameStatusPrecess(cpu::GameStatus status);
    // 发牌
    void startGiveCards();
    // 移动扑克牌
    void cardMove(Player* player, int movement);
    // 处理分发得到的扑克牌
    void disposeCards(Player* player, const Cards& cards);
    // 更新扑克牌在窗口中的显示
    void updatePlayerCards(Player* player);
    // 加载玩家名字
    void setPlayerName(QString name);
    // 定时器的处理动作
    void dispatchCards();
    // 处理玩家状态的变化
    void playerStatusChanged(Player* player, cpu::PlayerStatus status);
    // 处理玩家抢地主
    void disposeCallLord(Player* player, int bet, bool flag);
    // 处理玩家的出牌
    void disposePlayCards(Player* player, Cards& cards);
    // 处理玩家选牌
    void cardSelected(Qt::MouseButton button);
    // 处理用户玩家出牌
    void userPlayCards();
    // 隐藏玩家打出的牌
    void hidePlayerDropCards(Player* player);
    void showEndScene();

protected:
    void paintEvent(QPaintEvent* ev);
    void mouseMoveEvent(QMouseEvent* ev);

private:
    enum CardAlign { Horizontal, Vertical };
    struct PlayerContext {
        QRect cardsRect;  // 1. 玩家扑克牌显示的区域
        QRect playArea;  // 2. 出牌的区域

        CardAlign align;  // 3. 扑克牌的对齐方式(水平 or 垂直)
        bool isFront;  // 4. 扑克牌显示正面还是背面

        Cards lastCards;  // 5. 剩余手牌
        QLabel* myname; // 6. 玩家名字
    };

    QPushButton* myTempButton;
    QPixmap mysceneBack;

    QSize mycardSize;
    QRect mycardsRect;
    QRect myleftRect;
    QRect myrightRect;
    QPixmap mycardBackImg;
    QPixmap mycardFrontImg;
    QMap<Card, CardPanel*> mycardMap;

    CardPanel* mybaseCard;
    CardPanel* mymoveCard;
    QPoint mybaseCardPos;
    //QVector<CardPanel*> myTurtleCard;
    CardPanel* myTurtleCard;

    CardPanel* mycurSelCard;
    QSet<CardPanel*> myselectCards;
    QHash<CardPanel*, QRect> myuserCards;
    QHash<CardPanel*, QRect> leftCards;
    QHash<CardPanel*, QRect> rightCards;

    QMap<Player*, PlayerContext> mycontextMap;
    QVector<Player*> myplayerList;

    cpu* mycpu;
    cpu::GameStatus mygameStatus;
    QTimer* mytimer;
    QString NameNameName;
};

#endif // GAMESCENE_H
