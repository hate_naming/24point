#include "loading.h"

#include <QPainter>
#include <QTimer>

#include "gamescene.h"
#include "endscene.h"

Loading::Loading(QWidget *parent) : QWidget(parent)
{
    background.load("://image/loading_background.jpg");

    setWindowFlags(Qt::FramelessWindowHint | windowFlags()); //去窗口化
    setAttribute(Qt::WA_TranslucentBackground); //背景透明化

    QPixmap pixmap("://image/load.png");
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [=](){
        load = pixmap.copy(0, 0, countdown, pixmap.height());
        update();
        if(countdown >= pixmap.width())
        {
            timer->stop();
            timer->deleteLater();
            GameScene *m = new GameScene;
            m->show();
            close();
        }
        countdown += 3;
    });
    timer->start(15);
}

void Loading::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    p.drawPixmap(-100, 0, 800, 450, background);
    p.drawPixmap(64, 408, load.width(), load.height(), load);
}
