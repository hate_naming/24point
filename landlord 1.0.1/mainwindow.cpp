#include "mainwindow.h"
#include "ui_mainwindow.h"

mainWindow::mainWindow(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::mainWindow)
{
    ui->setupUi(this);
}

mainWindow::~mainWindow()
{
    delete ui;
}
