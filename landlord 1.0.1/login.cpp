#include "login.h"
#include "ui_login.h"
#include "mainwindow.h"
#include<QMessageBox>

login::login(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::login)
{
    ui->setupUi(this);
}

login::~login()
{
    delete ui;
}

static int n=3;
void login::on_pushButton_clicked()
{
   if(n-->=1)
   {
       if(ui->lineEdit->text()!="\0")
       {
           this->close();
           window = new mainWindow();
           window->show();
       }
       else
       {
           QMessageBox::information(NULL,"提示","请输入您的名字!");
       }
   }
   else
   {
       QMessageBox::information(NULL,"BYE BYE！","下次再一起玩吧！");
       this->close();
   }
}

void login::on_lineEdit_returnPressed()
{
    if(n-->=1)
    {
        if(ui->lineEdit->text()!="\0")
        {
            this->close();
            window = new mainWindow();
            window->show();
        }
        else
        {
            QMessageBox::information(NULL,NULL,"请输入您的名字!");
        }
    }
    else
    {
        QMessageBox::information(NULL,"BYE BYE！","下次再一起玩吧！");
        this->close();
    }
}
