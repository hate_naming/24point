#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void getPlayerName(QString name); // 载入玩家名称
    void setCards(); // 加载四个数字

    double cal(double x, double y, int n); // 对两个double进行运算
    bool judge(double a[], int n); // 判断数组是否可以算出24点
    bool judgeAll(double a[4]); // 判断全组合是否可以算出24点

    QString sign(int n); // 运算符的字符形式

    void paintEvent(QPaintEvent *); // 场景画面


private slots:
    void on_endButton_clicked();

    void on_renewButton_clicked();

    void on_tipButton_clicked();

    void on_yesButton_clicked();

    void on_noButton_clicked();

    void on_ansButton_clicked();

private:
    Ui::MainWindow *ui;
    QString PlayerName;
    double point[4];
    bool is24p;
    QPixmap picCow;
    QPixmap picNo;
    QString tempSolution; // 工具字符串
    QString solution=" "; // 一种解法
};
#endif // MAINWINDOW_H
