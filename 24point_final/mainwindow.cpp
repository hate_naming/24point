#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QPainter>
#include <QMessageBox>
#include <QRandomGenerator>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(800, 600);
    setCards();
//    double test[4]={2.0,2.0,2.0,3.0};
//    double test[4]={3.0,5.0,4.0,12.0};
//    double test[4]={4.0,7.0,10.0,11.0};
//    judgeAll(test);
//    qDebug() << solution;
//    qDebug() << "该数组" << judge(test,4);
//    qDebug() << "全组合"  << judgeAll(test);
//    solution = " ";
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::getPlayerName(QString name)
{
    PlayerName = name;
}

void MainWindow::setCards()
{
    int *num = new int[4];
    for(int i=0; i<4; i++){
        num[i] = QRandomGenerator::global()->bounded(15) + 1; // 随机生成1~15的数
    }
    for(int i=3; i>=1; i--){
        for(int j=1; j<=i; j++){
            if(num[j-1]>num[j]){
                int temp = num[j-1];
                num[j-1] = num[j];
                num[j] = temp;
            }
        }
    }
    for(int i=0; i<4; i++){
        point[i] = num[i]; // 正序载入数组
    }
    is24p = judgeAll(point);
    qDebug() << point[0] << point[1] << point[2] << point[3];
}

double MainWindow::cal(double x, double y, int n)
{
    if(n==1) return x+y;
    if(n==2) return x-y;
    if(n==3) return x*y;
    if(n==4) return x/y;
    return 0;
}

bool MainWindow::judge(double a[], int n) // 递归反馈是否可以算出24，同时记录一种可行的算法
{
    if(n==1){
        if(fabs(a[0]-24)<0.00000001) tempSolution+="24=";
        return fabs(a[0]-24)<0.00000001;
    }
    if(n==2){
        for(int i=1; i<=4; i++){
            double b[1];
            b[0] = cal(a[0],a[1],i);
            if(judge(b,1)){
                tempSolution += tr("%1").arg(a[0])+ sign(i) +"(";
                return 1;
            }
        }
        return 0;

    }
    if(n==3){
        for(int i=1; i<=4; i++){
            double b[2] = {a[0],a[1]};
            b[1] = cal(a[1],a[2],i);
            if(judge(b,2)){
                tempSolution += tr("%1").arg(a[1])+ sign(i) +"(";
                return 1;
            }
        }
        return 0;
    }
    if(n==4){
        for(int i=1; i<=4; i++){
            double b[3] = {a[0],a[1],a[2]};
            b[2] = cal(a[2],a[3],i);
            if(judge(b,3)){
                tempSolution += tr("%1").arg(a[2])+ sign(i) + tr("%1").arg(a[3]) +"))";
                return 1;
            }
        }
        return 0;
    }
    return 0;
}

bool MainWindow::judgeAll(double a[4]) // 给出所有可能的数组排序方式，再进行judge函数的判断
{
    int m[6][4]={0,1,2,3, 0,1,3,2, 0,2,1,3, 0,2,3,1, 0,3,1,2, 0,3,2,1};
    int sum=0;
    double t[24][4];
    for(int i=0;i<24;i++){
        double temp[4];
        for(int j=0;j<4;j++){
            t[i][j]=a[(i/6+m[i%6][j])%4];
            temp[j]=t[i][j];
        }
        if(judge(temp,4)){
            if(solution==" "){
                solution = tempSolution;
                qDebug() << tempSolution;
                tempSolution = '\0';
            }
            return 1;
        }
    }
    return 0;
}

QString MainWindow::sign(int n)
{
    if(n==1) return "+";
    if(n==2) return "-";
    if(n==3) return "×";
    if(n==4) return "÷";
    return 0;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing); // 抗锯齿
    for(int i=0; i<4; i++){
        p.drawPixmap(100+150*i, 150, 150, 250,"://picture/" + tr("%1").arg(point[i]) + ".png"); // 打印数字图片
    }
    p.drawPixmap(272, 456, 256, 144, picCow); // 打印cow.jpg
    p.drawPixmap(350, 478, 50, 50, picNo); // 打印No.png
}

void MainWindow::on_endButton_clicked() // 按下退出
{
    if (QMessageBox::Close  ==  QMessageBox::information(this,"","是否确认退出游戏？",QMessageBox::Close|QMessageBox::Cancel,QMessageBox::Cancel)){
        QMessageBox::information(this,"BYE BYE！","下次再一起玩吧！");
        close();
        qDebug() << "退出";
    }
    else{
        qDebug() << "取消";
    }
}

static int n = 6;
void MainWindow::on_renewButton_clicked() // 按下刷新
{
    if (QMessageBox::Yes  ==  QMessageBox::information(this,"","是否刷新卡牌？",QMessageBox::Yes|QMessageBox::Cancel,QMessageBox::Cancel)){
        solution = " ";
        n=6;
        picCow.load("");
        picNo.load("");
        setCards();
        update();
        qDebug() << "刷新";
    }
    else{
        qDebug() << "取消";
    }
}

void MainWindow::on_tipButton_clicked() // 按下玩法
{
    QMessageBox *box = new QMessageBox;
    box->setStandardButtons(QMessageBox::Close);
    box->setText("是否能通过“+-×÷”用已知的四个数得到24呢？");
    box->exec();
}

void MainWindow::on_yesButton_clicked() // 按下Yes
{
    //回答正确则打印牛牛的图片，否则给个叉叉（不会重复出现）
    QMessageBox *box = new QMessageBox;
    box->setFixedSize(200, 80);
    if(is24p){
        box->setWindowTitle("大拇哥");
        box->setText("恭喜"+PlayerName+"回答正确，确实可以");
        box->exec();
        if(picCow==picNo) picCow.load("://picture/cow.jpg");
    }
    else{
        box->setWindowTitle("寄咯");
        box->setText("答案是不能，别伤心，再来！");
        box->exec();
        if(picCow==picNo) picNo.load("://picture/No.png");
    }
}

void MainWindow::on_noButton_clicked() // 按下No！
{
    //回答正确则打印牛牛的图片，否则给个叉叉（不会重复出现）
    QMessageBox *box = new QMessageBox;
    box->setFixedSize(200, 80);
    if(!is24p){
        box->setWindowTitle("大拇哥");
        box->setText("恭喜"+PlayerName+"回答正确，确实不行");
        box->exec();
        if(picCow==picNo) picCow.load("://picture/cow.jpg");
    }
    else{
        box->setWindowTitle("寄咯");
        box->setText("答案是可以，要不再想想？！");
        box->exec();
        if(picCow==picNo) picNo.load("://picture/No.png");
    }
}

void MainWindow::on_ansButton_clicked() // 按下小可爱按钮
{
    //前五次进行交互，最后返回答案
    QMessageBox *box = new QMessageBox;
    box->setFixedSize(200, 80);
    if(n==5){
        box->setWindowTitle("shuizhaoderen");
        box->setText("别叫我，我还要睡！！！");
        box->exec();
    }
    if(n==3){
        box->setWindowTitle("haizaishuijiaoderen");
        box->setText("别烦我，自己玩去！！！");
        box->exec();
    }
    if(n==1){
        box->setWindowTitle(" ");
        box->exec();
    }
    if(n<=0){
        if(is24p){
            box->setWindowTitle("可能的解法:");
            box->setText("     "+solution+"     ");
        }
        else{
            box->setWindowTitle("凑不出来");
            box->setText("按钮被题目揍得重伤倒地");
        }
        box->exec();
    }
    n--;
}
