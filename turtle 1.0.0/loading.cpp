//#include "gamescene.h"
#include "loading.h"

#include <QPainter>
#include <QTimer>

Loading::Loading(QWidget *parent) : QWidget(parent)
{
    background.load("://image/loading_background.png");
    setFixedSize(background.size());
    loadback.load("://image/loadback.png");
    setFixedSize(loadback.size());
    // 去边框
    setWindowFlags(Qt::FramelessWindowHint | windowFlags());
    // 背景透明
    setAttribute(Qt::WA_TranslucentBackground);

    QPixmap pixmap("://image/load.png");
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [=](){
        load = pixmap.copy(0, 0, countdown, pixmap.height());
        update();
        if(countdown >= pixmap.width())
        {
            timer->stop();
            timer->deleteLater();
            GameScene* scene = new GameScene;
            scene->show();
            close();
        }
        countdown += 3;
    });
    timer->start(15);
}

void Loading::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter p(this);
    p.drawPixmap(rect(), background);
    //p.drawPixmap(62, 17, background.width(), background.height(), background);
    p.drawPixmap(62, 417, loadback.width(), loadback.height(), loadback);
    p.drawPixmap(62, 417, load.width(), load.height(), load);
}
