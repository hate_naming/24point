#include "player.h"

#include <QDebug>

Player::Player(QObject *parent) : QObject(parent){ mytype = Unknown; }

Player::Player(QString name, QObject *parent) : QObject(parent){
    myname = name;
    mytype = Unknown;
}

Player::Player(QString name, Player::Type playtype, QObject *parent) : QObject(parent){
    myname = name;
    mytype = playtype;
}

void Player::setName(QString name)
{
    myname = name;
}

QString Player::getName()
{
    return myname;
}

void Player::setType(Type type)
{
    mytype = type;
}

Player::Type Player::getType()
{
    return mytype;
}

void Player::setLose(bool flag)
{
    loseFlag = flag;
}

bool Player::isLose()
{
    return loseFlag;
}

void Player::setForePlayer(Player *player)
{
    myfore = player;
}

void Player::setNextPlayer(Player *player)
{
    mynext = player;
}

Player *Player::getForePlayer()
{
    return myfore;
}

Player *Player::getNextPlayer()
{
    return mynext;
}

void Player::storeDispatchCard(const Card& card)
{
    mycards.add(card);
    Cards cards;
    cards.add(card);
    emit notifyGiveCards(this, cards);
}

void Player::storeDispatchCards(const Cards& cards)
{
    mycards.add(cards);
    emit notifyGiveCards(this, cards);
}

Cards Player::getCards()
{
    return mycards;
}

void Player::clearCards()
{
    mycards.clear();
}

void Player::playCards(Cards &cards)
{
    mycards.remove(cards);
    emit notifyPlayCards(this, cards);
}

Player *Player::getPendPlayer()
{
    return mypendPlayer;
}

void Player::storePend(Player *player)
{
    mypendPlayer = player;
}

void prepareplay(){}
