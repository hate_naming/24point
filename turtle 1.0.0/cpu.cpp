#include "cpu.h"

#include <QTimer>
#include <QDebug>
#include <QRandomGenerator>

#include "card.h"
#include "player.h"

cpu::cpu(QObject *parent) : QObject(parent) {}

void cpu::setPlayer() {
    // 对象实例化
    myLeftRob = new Player("机器人A", this);
    myRightRob = new Player("机器人B",  this);
    myuser = new Player("我自己",  this);

    myuser->setForePlayer(myLeftRob);
    myuser->setNextPlayer(myRightRob);

    // left robot
    myLeftRob->setForePlayer(myRightRob);
    myLeftRob->setNextPlayer(myuser);

    // right robot
    myRightRob->setForePlayer(myuser);
    myRightRob->setNextPlayer(myLeftRob);

    // 指定当前玩家
    mycurrPlayer = (Player*)myuser;

    // 处理玩家出牌
    connect(myLeftRob, &Player::notifyPlayCards, this, &cpu::disposePlayCards);
    connect(myRightRob, &Player::notifyPlayCards, this, &cpu::disposePlayCards);
    connect(myuser, &Player::notifyPlayCards, this, &cpu::disposePlayCards);
}

Player *cpu::getLeftRobot() { return myLeftRob; }

Player *cpu::getRightRobot() { return myRightRob; }

Player *cpu::getUser() { return myuser; }

void cpu::setCurrentPlayer(Player *player) { mycurrPlayer = player; }

Player *cpu::getCurrentPlayer() { return mycurrPlayer; }

Player *cpu::getPendPlayer() { return mypendPlayer; }

Cards cpu::getPendCards() { return mypendCards; }

void cpu::setAllCards() {
    myallCards.clear();  //清空
    for (int point = Card::Point_3; point <= Card::Point_15; point++)  //每一个点数对应四个花色
    {
        for (int color = Card::Diamond; color <= Card::Spade; color++)  //枚举不能直接算数运算
        {
            Card card((Card::CardPoint)point, (Card::CardColor)color);  //强制转换回枚举类型
            myallCards.add(card);
        }
    }
}

Card cpu::takeOneCard() {
#if 0
    // 测试飞机
    static bool flag = true;
    static Cards temp;
    if(flag)
    {
        Card c1(Card::Point_10, Card::Club);
        Card c2(Card::Point_10, Card::Diamond);
        Card c3(Card::Point_10, Card::Heart);

        Card c4(Card::Point_J, Card::Club);
        Card c5(Card::Point_J, Card::Diamond);
        Card c6(Card::Point_J, Card::Heart);

        temp << c1 << c2 << c3 << c4 << c5 << c6;
        myallCards.remove(temp);
        flag = false;
    }

    if(getCurrentPlayer() == mtuser && !temp.isEmpty())
    {
        return tmp.takeRandomCard();
    }
    else
    {
        return myallCards.takeRandomCard();
    }
#else
    // not test code
    return myallCards.takeRandomCard();
#endif
}

//Cards cpu::getSurplusCards() { return myallCards; }

void cpu::resetCardData() {
    // 洗牌
    setAllCards();
    // 清空所有玩家的牌
    myLeftRob->clearCards();
    myRightRob->clearCards();
    myuser->clearCards();
    // 初始化出牌玩家和牌
    mypendPlayer = nullptr;
    mypendCards.clear();
}

void cpu::disposePlayCards(Player *player, Cards &card) {
    // 1. 将玩家出牌的信号转发给主界面
    emit notifyPlayHand(player, card);
    // 2. 如果不是空牌, 给其他玩家发送信号, 保存出牌玩家对象和打出的牌
    if (!card.isEmpty()) {
        mypendPlayer = player;
        emit pendingInfo(player, card);
    }
    // 3. 如果玩家的牌出完了, 计算本局游戏的总分
    Player *fore = player->getForePlayer();
    Player *next = player->getNextPlayer();
    if (player->getCards().isEmpty() && !(fore->getCards().isEmpty()&&next->getCards().isEmpty())) {
        next->setForePlayer(fore);
        fore->setNextPlayer(next);
        emit playerStatusChanged(mycurrPlayer, cpu::Winning);
        return;
    }
    if(fore->getCards().isEmpty()&&next->getCards().isEmpty()){
        emit playerStatusChanged(mycurrPlayer, cpu::Losing);
    }
    // 4. 牌没有出完, 下一个玩家继续出牌
    mycurrPlayer = player->getNextPlayer();
    mycurrPlayer->prepareplay();
    emit playerStatusChanged(mycurrPlayer, cpu::ThinkingForPlayCards);
}
