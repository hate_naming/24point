#ifndef ENDSCENE_H
#define ENDSCENE_H

#include <QLabel>
#include <QMainWindow>
#include <QPushButton>

class EndScene : public QMainWindow
{
    Q_OBJECT
public:
    EndScene();
    EndScene(QWidget *parent = nullptr);
    EndScene(bool isWin, QWidget *parent = nullptr);

signals:
    void continueGame();
    void endGame();

protected:
    void paintEvent(QPaintEvent* ev);

private:
    QPixmap background;
    QLabel* mytitle;
    QPushButton* mycontinue;
    QPushButton* myend;
};

#endif // ENDSCENE_H
