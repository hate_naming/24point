#ifndef CPU_H
#define CPU_H

#include <QTimer>
#include <QDebug>
#include <QObject>
#include <QRandomGenerator>

#include "card.h"
#include "player.h"


//游戏控制逻辑
class cpu : public QObject {
    Q_OBJECT
public:
    // 游戏状态
    enum GameStatus { DispatchCard, PlayingCards };
    // 玩家状态
    enum PlayerStatus {ThinkingForPlayCards, Winning, Losing };

    explicit cpu(QObject* parent = nullptr);

    // 初始化玩家
    void setPlayer();

    // 得到玩家的实例对象
    Player* getLeftRobot();
    Player* getRightRobot();
    Player* getUser();

    void setCurrentPlayer(Player* player);
    Player* getCurrentPlayer();

    // 得到出牌玩家和打出的牌
    Player* getPendPlayer();
    Cards getPendCards();
    void preparePlay();
    // 初始化扑克牌
    void setAllCards();
    // 每次发一张牌
    Card takeOneCard();
    // 重置卡牌数据
    void resetCardData();
    // 清空所有玩家的得分
    void clearPlayerScore();
    // 得到玩家下注的最高分数
    int getPlayerMaxBet();
    // 处理叫地主
    void disposeCallLord(Player* player, int bet);
    // 处理出牌
    void disposePlayCards(Player* player, Cards& card);


signals:
    void playerStatusChanged(Player* player, PlayerStatus status);
    // 游戏状态变化
    void gameStatusChanged(GameStatus status);
    // 通知玩家回合结束
    void notifyPlayHand(Player* player, Cards& card);
    // 传递玩家回合数据
    void pendingInfo(Player* player, Cards& card);

private:
    Player* myLeftRob;
    Player* myRightRob;
    Player* myuser;
    Player* mycurrPlayer = nullptr;  //当前玩家
    Player* mypendPlayer = nullptr;  //当前出牌玩家
    Cards mypendCards;
    Cards myallCards;
};



#endif // CPU_H
