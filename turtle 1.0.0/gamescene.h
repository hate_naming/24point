#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <QMap>
#include <QLabel>
#include <QMainWindow>

#include "cpu.h"
#include "card.h"
#include "cardpanel.h"

QT_BEGIN_NAMESPACE
namespace Ui {class GameScene;}
QT_END_NAMESPACE

class GameScene : public QMainWindow
{
    Q_OBJECT

public:
    GameScene(QWidget *parent = nullptr);
    ~GameScene();
    // 初始化游戏控制类信息
    void setcpu();
    // 切割并存储图片
    void setCardMap();
    // 裁剪图片
    void cropImage(QPixmap& pix, Card c);
    // 初始化游戏按钮组
    void setButtonsGroup();
    // 初始化玩家在窗口中的上下文环境
    void setPlayerContext();
    // 初始化游戏场景
    void setGameScene();
    // 处理游戏的状态
    void gameStatusPrecess(cpu::GameStatus status);
    // 发牌
    void startGiveCards();
    // 移动扑克牌
    void cardMove(Player* player, int movement);
    // 处理分发得到的扑克牌
    void disposeCards(Player* player, const Cards& cards);
    // 更新扑克牌在窗口中的显示
    void updatePlayerCards(Player* player);
    // 加载玩家名字
    void setPlayerName(QString name);
    // 定时器的处理动作
    void dispatchCards();
    // 处理玩家状态的变化
    void playerStatus(Player* player, cpu::PlayerStatus status);
    // 处理玩家抢地主
    void disposeCallLord(Player* player, int bet, bool flag);
    // 处理玩家的出牌
    void disposePlayCards(Player* player, Cards& cards);
    // 处理玩家选牌
    void cardSelected(Qt::MouseButton button);
    // 处理用户玩家出牌
    void userPlayCards();
    // 用户玩家放弃出牌
    void userPass();
    // 隐藏玩家打出的牌
    void hidePlayerDropCards(Player* player);
    // 显示玩家的最终得分
    void showEndingScorePanel();

protected:
    void paintEvent(QPaintEvent* ev);
    void mouseMoveEvent(QMouseEvent* ev);

private:
    enum CardAlign { Horizontal, Vertical };
    struct PlayerContext {
        // 1. 玩家扑克牌显示的区域
        QRect cardRect;
        // 2. 出牌的区域
        QRect playArea;
        // 3. 扑克牌的对齐方式(水平 or 垂直)
        CardAlign align;
        // 4. 扑克牌显示正面还是背面
        bool isFront;
        // 5. 游戏过程中的提示信息, 比如: 不出
        QLabel* info;
        // 6. 玩家的头像
        QLabel* roleImg;
        // 7. 玩家刚打出的牌
        Cards lastCards;
        // 8. 玩家名称
        QLabel* myname;
    };

    Ui::GameScene* myui;
    QPixmap mycardback;
    QSize mycardSize;
    QRect mycardsRect;
    QPixmap mycardBackImg;
    CardPanel* mybaseCard;
    CardPanel* mymoveCard;
    QPoint mybaseCardPos;
    QVector<CardPanel*> mylastThreeCard;
    CardPanel* mycurSelCard;
    QHash<CardPanel*, QRect> myuserCards;
    QMap<Card, CardPanel*> mycardMap;
    QSet<CardPanel*> myselectCards;
    QMap<Player*, PlayerContext> mycontextMap;
    QVector<Player*> myplayerList;
    cpu* mycpu;
    cpu::GameStatus mygameStatus;
    QTimer* mytimer;
    QString PlayerName;
};

#endif // GAMESCENE_H
