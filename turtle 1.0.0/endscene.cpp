#include "endscene.h"

#include <QPainter>

EndScene::EndScene(QWidget *parent) : QMainWindow(parent){}

EndScene::EndScene(bool isWin, QWidget *parent) : QMainWindow(parent)
{
    //background.load(":/images/gameover.png");
    //setFixedSize(background.size());

    // 显示用户玩家的角色以及游戏状态
    mytitle = new QLabel(this);
    if(isWin)
    {
        //mytitle->setPixmap(QPixmap(":/images/lord_win.png"));
    }
    else
    {
        mytitle->setPixmap(QPixmap(":/images/lose.jpg"));
    }
    mytitle->move(125, 125);

    // 继续游戏按钮
    mycontinue = new QPushButton(this);
    mycontinue->move(84, 405);
    mycontinue->setFixedSize(231, 48);
    connect(mycontinue, &QPushButton::clicked, this, &EndScene::continueGame);

    //结束游戏按钮
    myend = new QPushButton(this);
    myend->move(84, 453);
    mycontinue->setFixedSize(231, 48);
    connect(mycontinue, &QPushButton::clicked, this, close());
}

void EndScene::paintEvent(QPaintEvent *ev)
{
    Q_UNUSED(ev)
    QPainter p(this);
    p.drawPixmap(rect(), background);
}
