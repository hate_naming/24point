#ifndef LOADING_H
#define LOADING_H

#include <QWidget>

//加载动画

class Loading : public QWidget {
  Q_OBJECT
 public:
  explicit Loading(QWidget* parent = nullptr);

 signals:
 protected:
  void paintEvent(QPaintEvent* event);

 private:
  QPixmap background;
  QPixmap loadback;
  QPixmap load;
  int countdown = 15;
};

#endif // LOADING_H
