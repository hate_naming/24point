#include "card.h"

#include <QDebug>
#include <QRandomGenerator>
#include <QPainter>
#include <QMouseEvent>

Card::Card() {}

Card::Card(CardPoint point, CardColor color) {
    setPoint(point);
    setColor(color);
}

void Card::setPoint(CardPoint point) { mypoint = point; }

void Card::setColor(CardColor color) { mycolor = color; }

Card::CardPoint Card::point() const { return mypoint; }

Card::CardColor Card::color() const { return mycolor; }

bool smallPoint(const Card &c1, const Card &c2)  //由大到小排序
{
    if (c1.point() == c2.point())  //点数相同，判断花色
    {
        return c1.color() < c2.color();
    } else {
        return c1.point() < c2.point();
    }
}

bool bigPoint(const Card &c1, const Card &c2) {
    if (c1.point() == c2.point()) {
        return c1.color() > c2.color();
    } else {
        return c1.point() > c2.point();
    }
}

bool operator==(const Card &left, const Card &right) {
    return (left.point() == right.point() && left.color() == right.color());
}

uint qHash(const Card &card) { return card.point() * 100 + card.color(); }

bool operator<(const Card &c1, const Card &c2) { return smallPoint(c1, c2); }
bool operator>(const Card &c1, const Card &c2) { return bigPoint(c1, c2); }

Cards::Cards() {}

Cards::Cards(const Card &card) { add(card); }

void Cards::add(const Card &card) { mycardset.insert(card); }

void Cards::add(const Cards &cards) {
    mycardset.unite(cards.mycardset);  //合并Qset操作，存储到调用者中
}

void Cards::add(const QVector<Cards> &cards) {
    qDebug() << cards.count();
    for (int i = 0; i < cards.count(); i++) {
        add(cards.at(i));
    }
}

Cards &Cards::operator<<(const Card &card) {
    add(card);
    return *this;
}

Cards &Cards::operator<<(const Cards &cards) {
    add(cards);
    return *this;
}

void Cards::remove(const Card &card) { mycardset.remove(card); }

void Cards::remove(const Cards &cards) {
    mycardset.subtract(cards.mycardset);  //差集Qset操作，对原Qset 删除相同的元素
}

void Cards::remove(const QVector<Cards> &cards) {
    qDebug() << cards.size();
    for (int i = 0; i < cards.size(); i++) {
        remove(cards.at(i));
    }
}

int Cards::cardCount() { return mycardset.size(); }

bool Cards::isEmpty() { return mycardset.isEmpty(); }

void Cards::clear() { mycardset.clear(); }

Card::CardPoint Cards::maxPoint() {
    Card::CardPoint max = Card::Point_3;
    if (!mycardset.isEmpty()) {
        for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
            if (temp->point() > max) {
                max = temp->point();
            }
        }
    }
    return max;
}

Card::CardPoint Cards::minPoint() {
    Card::CardPoint min = Card::Point_15;
    if (!mycardset.isEmpty()) {
        for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
            if (temp->point() < min) {
                min = temp->point();
            }
        }
    }
    return min;
}

int Cards::pointCount(Card::CardPoint point)  //某一点数的个数
{
    int count = 0;
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        if (temp->point() == point) {
            count++;
        }
    }
    return count;
}

bool Cards::contains(const Card &card) { return mycardset.contains(card); }

bool Cards::contains(const Cards &cards) {
    return mycardset.contains(cards.mycardset);
}

Card Cards::takeRandomCard() {
    int num = QRandomGenerator::global()->bounded(mycardset.size());  //随机数0~max-1，左闭右开，可使用double
    QSet<Card>::const_iterator temp = mycardset.constBegin();  //只读迭代器
    for (int i = 0; i < num; i++, temp++);  //循环递增到指定的位置 后移num
    Card card = *temp;    //获取迭代器指向的牌的拷贝
    mycardset.erase(temp);  //删除该牌
    return card;        //返回拷贝
}

CardList Cards::toCardList(SetOrder type) {
    CardList list;
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        list << *temp;  //传入list
    }
    if (type == Asc) {  //升序
        //参数3 是一个返回类型为bool的回调函数 ，指定排序方式，左右符合
        //不换(true)，左右不符 换(false)
        std::sort(list.begin(), list.end(), smallPoint);
    } else if (type == Desc) {  //降序
        std::sort(list.begin(), list.end(), bigPoint);
    }
    return list;
}

void Cards::printCardDebug() {
    QString text;
    char p[] = "JQKA2";
    for (auto temp = mycardset.begin(); temp != mycardset.end(); temp++) {
        QString msg;
        Card::CardPoint point = temp->point();
        Card::CardColor color = temp->color();
        if (color == Card::CardColor::Club) { msg = "梅花"; }
        if (color == Card::CardColor::Diamond) { msg = "方片"; }
        if (color == Card::CardColor::Heart) { msg = "红桃"; }
        if (color == Card::CardColor::Spade){ msg = "黑桃"; }
        if (point >= Card::Point_3 && point <= Card::Point_10) {
            msg = QString("%1%2").arg(msg).arg(point + 2);
        } else if (point >= Card::Point_11 && point <= Card::Point_15) {
            msg = QString("%1%2").arg(msg).arg(p[point - Card::Point_11]);
        }
        msg += "  ";
        text += msg;
    }
    qDebug() << text;
}
