#include "gamescene.h"

#include <QDebug>
#include <QImage>
#include <QMouseEvent>
#include <QPainter>
#include <QPropertyAnimation>
#include <QRandomGenerator>
#include <QTimer>
#include <QPixmap>

#include "card.h"
#include "player.h"
#include "button.h"
#include "endscene.h"
#include "ui_gamescene.h"

GameScene::GameScene(QWidget* parent): QMainWindow(parent), myui(new Ui::GameScene) {
    myui->setupUi(this);

    //随机选取背景图
    int num = QRandomGenerator::global()->bounded(2);
    QString path = QString(":/images/background-%1.jpg").arg(num + 1);
    mycardBackImg.load(path);

    //窗口的标题的大小
    this->setWindowTitle("乌龟");
    this->setFixedSize(1000, 650);

    //设置游戏控制
    setcpu();

    //卡牌图片
    setCardMap();

    //初始化游戏中的按钮组
    setButtonsGroup();

    //初始化玩家在窗口中的上下文环境(牌组，头像的位置)
    setPlayerContext();

    //卡牌场景初始化
    setGameScene();
}

GameScene::~GameScene() { delete myui; }

void GameScene::setcpu() {
    mycpu = new cpu(this);
    mycpu->setPlayer();
    // 得到三个玩家的实例对象
//    Robot* leftRobot = mycpu->getLeftRobot();
//    Robot* rightRobot = mycpu->getRightRobot();
//    User* user = mycpu->getUserPlayer();
    Player* leftRobot;
    Player* rightRobot;
    Player* user;
    QVector<Player*> tempplayer;
    // 存储的顺序: 左侧机器人, 右侧机器人, 当前玩家
    myplayerList << leftRobot << rightRobot << user;
    connect(mycpu, &cpu::playerStatusChanged, this,
            &GameScene::playerStatus);
    connect(mycpu, &cpu::gameStatusChanged, this,
            &GameScene::gameStatusPrecess);
    connect(mycpu, &cpu::notifyPlayHand, this,
            &GameScene::disposePlayCards);

    connect(leftRobot, &Player::notifyGiveCards, this, &GameScene::disposeCards);
    connect(rightRobot, &Player::notifyGiveCards, this, &GameScene::disposeCards);
    connect(user, &Player::notifyGiveCards, this, &GameScene::disposeCards);
}

void GameScene::setCardMap() {
    QString str1[4]={"diamond","club","heart","spade"};
    QString str2[13]={"point_3","point_4","point_5","point_6","point_7","point_8","point_9",
                      "point_10","point_11","point_12","point_13","point_14","point_15"};
    QPixmap backpic = QPixmap(":/images/rear.png");
    mycardBackImg = backpic;
    for (int i = 0, color = Card::Diamond; color <= Card::Spade; color++, i++) {
        for (int j = 0, point = Card::Point_3; point <= Card::Point_15; point++, j++) {
            Card card((Card::CardPoint)point, (Card::CardColor)color);  //强转
            QPixmap pixmap = QPixmap(":/images/"+str1[i]+"_"+str2[j]+".png");
            cropImage(pixmap, card);
        }
    }
}

void GameScene::cropImage(QPixmap &pix, Card c) {
    CardPanel* panel = new CardPanel(this);
    mycardSize.setWidth(mycardBackImg.width());
    mycardSize.setHeight(mycardBackImg.height());
    panel->setImage(pix, mycardBackImg);
    panel->setCard(c);
    panel->hide();
    mycardMap.insert(c, panel);
    connect(panel, &CardPanel::cardSelected, this, &GameScene::cardSelected);
}

//初始化 底部 按钮组
//void GameScene::setButtonsGroup() {
//    myui->buttonStack->setButtons();
//    myui->buttonStack->selectPanel(button::Start);  //选择栈窗口组显示的窗口

//    connect(myui->btnGroup, &ButtonGroup::startGame, this, [=]() {
//        // 界面的初始化
//        myui->btnGroup->selectPanel(button::Empty);
//        // 修改游戏状态 -> 发牌
//        gameStatusPrecess(cpu::);
//    });
//    //绑定按钮组信号
//    connect(myui->btnGroup, &ButtonGroup::playHand, this,&GameScene::userPlayCards);
//    connect(myui->btnGroup, &ButtonGroup::pass, this, &GameScene::userPass);
//    connect(myui->btnGroup, &ButtonGroup::betPoint, this, [=](int bet) {
//        mycpu->getUserPlayer()->callLord(bet);
//        myui->btnGroup->selectPanel(ButtonGroup::Empty);
//    });
//}

//void GameScene::setPlayerContext() {
//    // 1. 放置玩家扑克牌的区域
//    const QRect cardsRect[] = {
//        // x, y, width, height
//        QRect(90, 130, 100, height() - 200),                    // 左侧机器人
//        QRect(rect().right() - 190, 130, 100, height() - 200),  // 右侧机器人
//        QRect(250, rect().bottom() - 120, width() - 500, 100)   // 当前玩家
//    };
//    // 2.玩家出牌区域
//    const QRect playHandRect[] = {
//        QRect(260, 150, 100, 100),                             // 左侧机器人
//        QRect(rect().right() - 360, 150, 100, 100),            // 右侧机器人
//        QRect(150, rect().bottom() - 290, width() - 300, 105)  // 当前玩家
//    };
//    // 3.玩家头像区域
//    const QPoint roleImgPos[] = {
//        QPoint(cardsRect[0].left() - 80,
//        cardsRect[0].height() / 2 + 20),  // 左侧机器人
//        QPoint(cardsRect[1].right() + 10,
//        cardsRect[1].height() / 2 + 20),  // 右侧机器人
//        QPoint(cardsRect[2].right() - 10, cardsRect[2].top() - 10)  // 当前玩家
//    };

//    // 循环，将上下文信息传递给各个玩家
//    int index = myplayerList.indexOf(mycpu->getUserPlayer());  //获取的玩家对象的索引值
//    for (int i = 0; i < myplayerList.size(); ++i) {
//        PlayerContext context;
//        context.align =
//                i == index ? Horizontal : Vertical;  //设置布局 玩家水平 机器人竖直
//        context.isFront =
//                i == index ? true : false;    //玩家正面显示 机器人反面显示
//        context.cardRect = cardsRect[i];  //指定卡牌存放区域
//        context.playArea = playHandRect[i];  //出牌区域
//        // 提示信息 例如：不出
//        context.info = new QLabel(this);
//        context.info->resize(160, 98);
//        context.info->hide();  //先隐藏
//        // 提示信息 显示到出牌区域的中心位置
//        QRect rect = playHandRect[i];  //出牌区域
//        QPoint point(rect.left() + (rect.width() - context.info->width()) / 2,
//                  rect.top() + (rect.height() - context.info->height()) / 2);
//        context.info->move(point);
//        // 玩家的头像
//        context.roleImg = new QLabel(this);
//        context.roleImg->resize(84, 120);
//        context.roleImg->hide();
//        context.roleImg->move(roleImgPos[i]);
//        mycontextMap.insert(myplayerList.at(i), context);  //存入map中 key=玩家 value=上下文环境
//        // 玩家名称
//        context.myname = new QLabel(PlayerName);
//        context.myname->resize(184, 50);
//        context.myname->hide();
//        context.myname->move(roleImgPos[i].x(),roleImgPos[i].y()-50);
//    }
//}

//void GameScene::setGameScene() {
//    // 1. 发牌区的扑克牌
//    mybaseCard = new CardPanel(this);
//    mybaseCard->setImage(mycardBackImg, mycardBackImg);  //不需要显示证明
//    // 2. 发牌过程中移动的扑克牌
//    mymoveCard = new CardPanel(this);
//    mymoveCard->setImage(mycardBackImg, mycardBackImg);  //不需要显示正面
//    // 3. 最后的三张底牌(用于窗口的显示)
//    for (int i = 0; i < 3; ++i) {
//        CardPanel* panel = new CardPanel(this);
//        panel->setImage(mycardBackImg, mycardBackImg);  //不需要显示正面
//        mylastThreeCard.push_back(panel);
//        panel->hide();
//    }
//    // 发牌扑克牌的位置
//    mybaseCardPos = QPoint((width() - mycardSize.width()) / 2, height() / 2 - 100);
//    mybaseCard->move(mybaseCardPos);
//    mymoveCard->move(mybaseCardPos);  //起始位置

//    int base = (width() - 3 * mycardSize.width() - 2 * 10) /
//            2;  //剩余三张牌组的x轴起始坐标
//    for (int i = 0; i < 3; ++i) {
//        mylastThreeCard[i]->move(base + (mycardSize.width() + 10) * i, 20);
//    }
//}

//void GameScene::gameStatusPrecess(cpu::GameStatus status) {
//    // 记录游戏状态
//    mygameStatus = status;
//    // 处理游戏状态
//    switch (status) {
//    case cpu::DispatchCard:
//        startGiveCards();
//        break;
//    case cpu::CallingLord: {
//        // 取出底牌数据
//        CardList last3Card = mycpu->getSurplusCards().toCardList();
//        // 给底牌窗口设置图片
//        for (int i = 0; i < last3Card.size(); ++i) {
//            QPixmap front = mycardMap[last3Card.at(i)]->getImage();
//            mylastThreeCard[i]->setImage(front, mycardBackImg);
//            mylastThreeCard[i]->hide();
//        }
//        // 开始叫地主
//        mycpu->startLordCard();
//        break;
//    }
//    case cpu::PlayingHand:
//        // 隐藏发牌区的底牌和移动的牌
//        mybaseCard->hide();
//        mymoveCard->hide();
//        // 显示留给地主的三张底牌
//        for (int i = 0; i < mylastThreeCard.size(); ++i) {
//            mylastThreeCard.at(i)->show();
//        }
//        for (int i = 0; i < myplayerList.size(); ++i) {
//            PlayerContext& context = mycontextMap[myplayerList.at(i)];
//            // 隐藏各个玩家抢地主过程中的提示信息
//            context.info->hide();
//            // 显示各个玩家的头像
//            Player* player = myplayerList.at(i);
//            QPixmap pixmap = loadRoleImage(player->getSex(), player->getDirection(), player->getRole());
//            context.roleImg->setPixmap(pixmap);
//            context.roleImg->show();
//        }
//        break;
//    default:
//        break;
//    }
//}
////发牌
//void GameScene::startGiveCards() {
//    // 重置每张卡牌的属性
//    for (auto it = mycardMap.begin(); it != mycardMap.end(); ++it) {
//        it.value()->setSeclected(false);  //设置卡牌选中状态
//        it.value()->setFrontSide(true);   //设置卡牌显示面
//        it.value()->hide();               //隐藏
//    }
//    // 隐藏三张底牌
//    for (int i = 0; i < mylastThreeCard.size(); ++i) {
//        mylastThreeCard.at(i)->hide();
//    }
//    // 重新发牌，需要重置玩家的窗口上下文信息
//    int index = myplayerList.indexOf(mycpu->getUserPlayer());
//    for (int i = 0; i < myplayerList.size(); ++i) {
//        mycontextMap[myplayerList.at(i)].lastCards.clear();
//        mycontextMap[myplayerList.at(i)].info->hide();
//        mycontextMap[myplayerList.at(i)].roleImg->hide();
//        mycontextMap[myplayerList.at(i)].isFront = i == index ? true : false;
//    }
//    // 重置所有玩家的卡牌数据
//    mycpu->resetCardData();
//    // 显示底牌
//    mycpu->show();
//    // 隐藏按钮面板
//    myui->buttonStack->selectPanel(button::Empty);
//    // 启动定时器
//    mytimer->start(10);
//}

//单步移动
void GameScene::cardMove(Player* player, int curPos) {
    // 得到每个玩家的扑克牌展示区域
    QRect cardRect = mycontextMap[player].cardRect;
    // 每个玩家的单元步长
    const int unit[] = {(mybaseCardPos.x() - cardRect.right()) / 100,
                        (cardRect.left() - mybaseCardPos.x()) / 100,
                        (cardRect.top() - mybaseCardPos.y()) / 100};
    // 每次窗口移动的时候每个玩家对应的牌的实时坐标位置，curpos记录移动的步骤数
    const QPoint pos[] = {
        QPoint(mybaseCardPos.x() - curPos * unit[0], mybaseCardPos.y()),
        QPoint(mybaseCardPos.x() + curPos * unit[1], mybaseCardPos.y()),
        QPoint(mybaseCardPos.x(), mybaseCardPos.y() + curPos * unit[2]),
    };

    // 移动扑克牌窗口
    int index = myplayerList.indexOf(player);
    mymoveCard->move(pos[index]);

    // 临界状态处理
    if (curPos == 0) {
        mymoveCard->show();  //显示移动牌
    }
    if (curPos == 100) {
        mymoveCard->hide();
    }
}

void GameScene::disposeCards(Player* player, const Cards& cards) {
    Cards& myCard = const_cast<Cards&>(cards);
    CardList list = myCard.toCardList();
    for (int i = 0; i < list.size(); ++i) {
        CardPanel* panel = mycardMap[list.at(i)];
        panel->setOwner(player);
    }
    // 更新扑克牌在窗口中的显示
    updatePlayerCards(player);
}

void GameScene::updatePlayerCards(Player* player) {
    Cards cards = player->getCards();
    CardList list = cards.toCardList();

    mycardsRect = QRect();
    myuserCards.clear();
    // 取出展示扑克牌的区域（宽度）
    int cardSpace = 20;
    QRect cardsRect = mycontextMap[player].cardRect;
    for (int i = 0; i < list.size(); ++i) {
        CardPanel* panel = mycardMap[list.at(i)];
        panel->show();
        panel->raise();  //设置当前窗口上层显示
        panel->setFrontSide(mycontextMap[player].isFront);

        // 水平 or 垂直显示
        if (mycontextMap[player].align == Horizontal) {  //水平显示，用户牌组
            int leftX =
                    cardsRect.left() +
                    (cardsRect.width() - (list.size() - 1) * cardSpace - panel->width()) / 2;
            int topY =
                    cardsRect.top() + (cardsRect.height() - mycardSize.height()) / 2;
            if (panel->isSelected()) {  //牌被选中，弹出效果
                topY -= 10;
            }
            panel->move(leftX + cardSpace * i, topY);
            mycardsRect = QRect(leftX, topY, cardSpace * i + mycardSize.width(),mycardSize.height());
            int curWidth = 0;
            if (list.size() - 1 == i) {
                curWidth = mycardSize.width();
            } else {
                curWidth = cardSpace;
            }
            QRect cardRect(leftX + cardSpace * i, topY, curWidth, mycardSize.height());
            myuserCards.insert(panel, cardRect);
        } else {  //垂直显示，机器人牌组
            int leftX = cardsRect.left() + (cardsRect.width() - mycardSize.width()) / 2;
            int topY = cardsRect.top() + (cardsRect.height() - (list.size() - 1) * cardSpace - panel->height()) / 2;
            panel->move(leftX, topY + i * cardSpace);
        }
    }

    // 显示玩家打出的牌
    // 得到当前玩家的出牌区域以及本轮打出的牌
    QRect playCardRect = mycontextMap[player].playArea;
    Cards lastCards = mycontextMap[player].lastCards;
    if (!lastCards.isEmpty()) {
        int playSpacing = 24;
        CardList lastCardList = lastCards.toCardList();
        CardList::ConstIterator itplayed = lastCardList.constBegin();
        for (int i = 0; itplayed != lastCardList.constEnd(); ++itplayed, i++) {
            CardPanel* panel = mycardMap[*itplayed];
            panel->setFrontSide(true);
            panel->raise();
            // 将打出的牌移动到出牌区域
            if (mycontextMap[player].align == Horizontal) {
                int leftBase = playCardRect.left() + (playCardRect.width() -
                         (lastCardList.size() - 1) * playSpacing - panel->width()) / 2;
                int top = playCardRect.top() + (playCardRect.height() - panel->height()) / 2;
                panel->move(leftBase + i * playSpacing, top);
            } else {
                int left = playCardRect.left() + (playCardRect.width() - panel->width()) / 2;
                int top = playCardRect.top();
                panel->move(left, top + i * playSpacing);
            }
            panel->show();
        }
    }
}

//QPixmap GameScene::loadRoleImage(Player::Sex sex, Player::Direction direct, Player::Role role) {
//    // 找图片
//    QVector<QString> lordMan;
//    QVector<QString> lordWoman;
//    QVector<QString> farmerMan;
//    QVector<QString> farmerWoman;
//    lordMan << ":/images/lord_man_1.png" << ":/images/lord_man_2.png";
//    lordWoman << ":/images/lord_woman_1.png" << ":/images/lord_woman_2.png";
//    farmerMan << ":/images/farmer_man_1.png" << ":/images/farmer_man_2.png";
//    farmerWoman << ":/images/farmer_woman_1.png" << ":/images/farmer_woman_2.png";

//    // 加载图片  QImage
//    QImage image;
//    int random = QRandomGenerator::global()->bounded(2);
//    if (sex == Player::Man && role == Player::Lord) {
//        image.load(lordMan.at(random));
//    } else if (sex == Player::Man && role == Player::Farmer) {
//        image.load(farmerMan.at(random));
//    } else if (sex == Player::Woman && role == Player::Lord) {
//        image.load(lordWoman.at(random));
//    } else if (sex == Player::Woman && role == Player::Farmer) {
//        image.load(farmerWoman.at(random));
//    }

//    QPixmap pixmap;
//    if (direct == Player::Left) {
//        pixmap = QPixmap::fromImage(image);
//    } else {
//        pixmap = QPixmap::fromImage(image.mirrored(true, false));
//    }
//    return pixmap;
//}

//void GameScene::setPlayerName(QString name){
//    PlayerName = name;
//}

////会被计时器信号触发调用
//void GameScene::dispatchCards() {
//    // 记录扑克牌的位置（移动的步数，需要移动100步）
//    static int curMovePos = 0;
//    // 当前玩家
//    Player* curPlayer = mycpu->getCurrentPlayer();
//    if (curMovePos >= 100) {
//        // 给玩家发一张牌
//        Card card = mycpu->takeOneCard();
//        curPlayer->storeDispatchCard(card);  //存储到用户牌组中
//        // Cards cs(card);//little change, i think it useless
//        // disposeCard(curPlayer, cs);
//        // 切换玩家
//        mycpu->setCurrentPlayer(curPlayer->getNextPlayer());
//        curMovePos = 0;
//        // 发牌动画
//        cardMove(curPlayer, curMovePos);  //步数为0 ，显示牌
//        // 判断牌是否发完了
//        if (mycpu->getSurplusCards().cardCount() == 3) {  //留三张
//            // 终止定时器
//            mytimer->stop();
//            // 切换游戏状态
//            gameStatusPrecess(cpu::CallingLord);
//            // 终止发牌音乐的播放
//            return;
//        }
//    }
//    // 移动扑克牌
//    cardMove(curPlayer, curMovePos);
//    curMovePos += 15;  //每次计时器信号 移动15步
//}

//void GameScene::playerStatus(Player* player, cpu::PlayerStatus status) {
//    switch (status) {
//    case cpu::ThinkingForCallLord:
//        if (player == mycpu->getUserPlayer()) {
//            myui->btnGroup->selectPanel(ButtonGroup::CallLord,
//                                      mycpu->getPlayerMaxBet());
//        }
//        break;
//    case cpu::ThinkingForPlayHand:
//        // 1. 隐藏上一轮打出的牌
//        hidePlayerDropCards(player);
//        if (player == mycpu->getUserPlayer()) {
//            // 取出出牌玩家的对象
//            Player* pendPlayer = mycpu->getPendPlayer();
//            if (pendPlayer == mycpu->getUserPlayer() || pendPlayer == nullptr) {
//                myui->btnGroup->selectPanel(ButtonGroup::PlayCard);
//            } else {
//                myui->btnGroup->selectPanel(ButtonGroup::PassOrPlay);
//            }
//        } else {
//            myui->btnGroup->selectPanel(ButtonGroup::Empty);
//        }
//        break;
//    case cpu::Winning:
//        mycontextMap[mycpu->getLeftRobot()].isFront = true;
//        mycontextMap[mycpu->getRightRobot()].isFront = true;
//        updatePlayerCards(mycpu->getLeftRobot());
//        updatePlayerCards(mycpu->getRightRobot());
//        mycpu->setCurrentPlayer(player);
//        break;
//    default:
//        break;
//    }
//}

void GameScene::disposeCallLord(Player* player, int bet, bool flag) {
    // 显示抢地主的信息提示
    PlayerContext context = mycontextMap[player];
    if (bet == 0) {
        context.info->setPixmap(QPixmap(":/images/buqinag.png"));
    } else {
        if (flag) {
            context.info->setPixmap(QPixmap(":/images/jiaodizhu.png"));
        } else {
            context.info->setPixmap(QPixmap(":/images/qiangdizhu.png"));
        }
        // 显示叫地主的分数
        //showAnimation(Bet, bet);
    }
    context.info->show();
}

//void GameScene::disposePlayCards(Player* player, Cards& cards) {
//    // 存储玩家打出的牌
//    auto it = mycontextMap.find(player);
//    it->lastCards = cards;
//    // 2. 根据牌型播放游戏特效
//    CardGroup group(cards);
//    CardGroup::PlayType type = group.getPlayType();

//    // 如果玩家打出的是空牌(不出牌), 显示提示信息
//    if (cards.isEmpty()) {
//        it->info->setPixmap(QPixmap(":/images/pass.png"));
//        it->info->show();
//    }
//    // 3. 更新玩家剩余的牌
//    updatePlayerCards(player);
//}

//void GameScene::cardSelected(Qt::MouseButton button) {
//    // 1. 判断是不是出牌状态
//    if (mygameStatus == cpu::DispatchCard ||
//            mygameStatus == cpu::CallingLord) {
//        return;
//    }
//    // 2. 判断发出信号的牌的所有者是不是当前用户玩家
//    CardPanel* panel = static_cast<CardPanel*>(sender());
//    if (panel->getOwner() != mycpu->getUserPlayer()) {
//        return;
//    }
//    // 3. 保存当前被选中的牌的窗口对象
//    mycurSelCard = panel;
//    // 4. 判断参数的鼠标键是左键还是右键
//    if (button == Qt::LeftButton) {
//        // 设置扑克牌的选中状态
//        panel->setSeclected(!panel->isSelected());
//        // 更新扑克牌在窗口中的显示
//        updatePlayerCards(panel->getOwner());
//        // 保存或删除扑克牌窗口对象
//        QSet<CardPanel*>::const_iterator it = myselectCards.find(panel);
//        if (it == myselectCards.constEnd()) {
//            myselectCards.insert(panel);
//        } else {
//            myselectCards.erase(it);
//        }
//    } else if (button == Qt::RightButton) {
//        // 调用出牌按钮的槽函数
//        userPlayCards();
//    }
//}

//void GameScene::userPlayCards() {
//    // 判断游戏状态
//    if (mygameStatus != cpu::PlayingHand) {
//        return;
//    }
//    // 判断玩家是不是用户玩家
//    if (mycpu->getCurrentPlayer() != mycpu->getUserPlayer()) {
//        return;
//    }
//    // 判断要出的牌是否为空
//    if (myselectCards.isEmpty()) {
//        return;
//    }
//    // 得到要打出的牌的牌型
//    Cards cs;
//    for (auto temp = myselectCards.begin(); temp != myselectCards.end(); ++temp) {
//        Card card = (*temp)->getCard();
//        cs.add(card);
//    }
//    CardGroup group(cs);
//    CardGroup::PlayType type = group.getPlayType();
//    if (type == CardGroup::Type_Unknown) {
//        return;
//    }
//    // 判断当前玩家的牌能不能压住上一家的牌
//    if (mycpu->getPendPlayer() != mycpu->getUserPlayer()) {
//        Cards cards = mycpu->getPendCards();
//        if (!group.compare(CardGroup(cards))) {
//            return;
//        }
//    }
//    // 通过玩家对象出牌
//    mycpu->getUserPlayer()->CardGroup(cs);
//    // 清空容器
//    myselectCards.clear();
//}

//void GameScene::userPass() {
//    // 判断是不是用户玩家
//    Player* curPlayer = mycpu->getCurrentPlayer();
//    Player* userPlayer = mycpu->getUserPlayer();
//    if (curPlayer != userPlayer) {
//        return;
//    }
//    // 判断当前用户玩家是不是上一次出牌的玩家(可以不处理)
//    Player* pendPlayer = mycpu->getPendPlayer();
//    if (pendPlayer == userPlayer || pendPlayer == nullptr) {
//        return;
//    }
//    // 打出一个空的Cards对象
//    Cards blank;
//    userPlayer->playHand(blank);
//    // 清空用户选择的牌(玩家可能选择了一些牌, 但是没有打出去)
//    for (auto temp = myselectCards.begin(); temp != myselectCards.end(); ++temp) {
//        (*temp)->setSeclected(false);
//    }
//    myselectCards.clear();
//    // 更新玩家待出牌区域的牌
//    updatePlayerCards(userPlayer);
//}

//void GameScene::hidePlayerDropCards(Player* player) {
//    auto it = mycontextMap.find(player);
//    if (it != mycontextMap.end()) {
//        if (it->lastCards.isEmpty()) {
//            it->info->hide();
//        } else {
//            // Cards --> Card
//            CardList list = it->lastCards.toCardList();
//            for (auto last = list.begin(); last != list.end(); last++) {
//                mycardMap[*last]->hide();
//            }
//        }
//        it->lastCards.clear();
//    }
//}

//void GameScene::paintEvent(QPaintEvent* ev) {
//    Q_UNUSED(ev)
//    QPainter p(this);
//    p.drawPixmap(rect(), mycardBackImg);
//}

void GameScene::mouseMoveEvent(QMouseEvent* ev) {
    Q_UNUSED(ev)
    if (ev->buttons() & Qt::LeftButton) {
        QPoint pt = ev->pos();
        if (!mycardsRect.contains(pt)) {
            mycurSelCard = nullptr;
        } else {
            QList<CardPanel*> list = myuserCards.keys();
            for (int i = 0; i < list.size(); i++) {
                CardPanel* panel = list.at(i);
                if (myuserCards[panel].contains(pt) && mycurSelCard != panel) {
                    // 点击这张扑克牌
                    panel->clicked();
                    mycurSelCard = panel;
                }
            }
        }
    }
}
