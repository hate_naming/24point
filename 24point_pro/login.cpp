#include "login.h"
#include "ui_login.h"

#include <QDebug>
#include <QMessageBox>

#include "mainwindow.h"

Login::Login(QWidget *parent): QMainWindow(parent), ui_login(new Ui::login)
{
    ui_login->setupUi(this);
//    setFixedSize(781, 586);
//    ui_login->label_title->setGeometry(210, 70, 361, 51);
//    ui_login->label_cn->setGeometry(330, 150, 121, 41);
//    ui_login->label_tip->setGeometry(280, 280, 221, 41);
//    ui_login->lineEdit->setGeometry(290, 310, 401, 41);
//    ui_login->pushButton->setGeometry(340, 420, 101, 41);
}

Login::~Login()
{
    delete ui_login;
}

static int n=3;

void Login::on_pushButton_clicked()
{
    qDebug() << "点击按钮";
   if(n-->=1||ui_login->lineEdit->text()!="\0")
   {
       if(ui_login->lineEdit->text()!="\0")
       {
           QString Name = ui_login->lineEdit->text();
           this->close();
           MainWindow *w = new MainWindow;
           w->getPlayerName(Name);
           w->show();
       }
       else
       {
           QMessageBox::information(this,"提示","请输入您的名字!");
       }
   }
   else
   {
       QMessageBox::critical(this,"BYE BYE！","下次再一起玩吧！");
       this->close();
   }
}

void Login::on_lineEdit_returnPressed()
{
    qDebug() << "按下回车";
    if(n-->=1||ui_login->lineEdit->text()!="\0")
    {
        if(ui_login->lineEdit->text()!="\0")
        {
            QString Name = ui_login->lineEdit->text();
            this->close();
            MainWindow *w = new MainWindow;
            w->getPlayerName(Name);
            w->show();
        }
        else
        {
            QMessageBox::information(this,"提示","请输入您的名字!");
        }
    }
    else
    {
        QMessageBox::critical(this,"BYE BYE！","下次再一起玩吧！");
        this->close();
    }
}
