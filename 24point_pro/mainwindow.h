#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void getPlayerName(QString name);
    void setCards();
    void setScene();

    double cal(double x, double y, int n);
    bool judge(double a[], int n);
    bool judgeAll(double a[4]);

    QString sign(int n);

    void paintEvent(QPaintEvent *);


private slots:
    void on_endButton_clicked();

    void on_renewButton_clicked();

    void on_tipButton_clicked();

    void on_yesButton_clicked();

    void on_noButton_clicked();

    void on_ansButton_clicked();

private:
    Ui::MainWindow *ui;
    QString PlayerName;
    double point[4];
    bool is24p;
    QPixmap picCow;
    QPixmap picNo;
    QString tempSolution;
    QString solution=" ";
    QLabel *top = nullptr, *butt = nullptr;
};
#endif // MAINWINDOW_H
